<?php
$key = $_GET['key'];
include '../../../../lib/conn.php';

$page = isset($_POST['page']) ? intval($_POST['page']) : 1;
$rows = isset($_POST['rows']) ? intval($_POST['rows']) : 10;
$msearchdetail = isset($_POST['msearchdetail']) ? mysql_real_escape_string($_POST['msearchdetail']) : '';

$offset = ($page-1)*$rows;

$result = array();

$where = "IFNULL(inventoryname,'') like '%$msearchdetail%'";
$rs = mysql_query("select count(*) from purchaseorderdet LEFT JOIN inventory ON purchaseorderdet.inventoryid = inventory.idinventory where " . $where);
$row = mysql_fetch_row($rs);
$result["total"] = $row[0];

$rs = mysql_query("SELECT
					purchaseorderdet.idtransdet,
					purchaseorderdet.transid,
					purchaseorderdet.inventoryid,
					purchaseorderdet.unit,
					FORMAT(purchaseorderdet.unitprice,0) AS unitprice,
					purchaseorderdet.discount,
					purchaseorderdet.ppn,
					FORMAT(purchaseorderdet.amount,0) AS amount,
					purchaseorderdet.quantity,
					inventory.inventoryname
					FROM
					purchaseorderdet
					LEFT JOIN inventory ON purchaseorderdet.inventoryid = inventory.idinventory
					WHERE purchaseorderdet.transid=".$key." AND " . $where . " limit $offset,$rows");
$items = array();
while($row = mysql_fetch_object($rs)){
	array_push($items, $row);
}
$result["rows"] = $items;


// SUM() for footer --------------------
$rs = mysql_query("SELECT 'Total' AS unitprice, FORMAT(SUM(quantity),0) AS quantity, CASE WHEN purchaseorder.noppnflag = 1 THEN FORMAT(SUM(amount),0) ELSE FORMAT(SUM((amount)+(amount*0.1)),0) END AS amount 
FROM purchaseorderdet LEFT JOIN inventory ON purchaseorderdet.inventoryid = inventory.idinventory 
INNER JOIN purchaseorder ON purchaseorderdet.transid = purchaseorder.idtrans
WHERE purchaseorderdet.transid=".$key." AND " . $where . " limit $offset,$rows");

$totalitems = array();
while($row = mysql_fetch_object($rs)){

	array_push($totalitems, $row);			
}
$result["footer"] = $totalitems;
echo json_encode($result);
?>