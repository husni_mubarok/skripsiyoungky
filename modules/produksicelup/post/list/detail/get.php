<?php
$key = $_GET['key'];
include '../../../../../lib/conn.php';

$page = isset($_POST['page']) ? intval($_POST['page']) : 1;
$rows = isset($_POST['rows']) ? intval($_POST['rows']) : 10;
$msearchdetail = isset($_POST['msearchdetail']) ? mysql_real_escape_string($_POST['msearchdetail']) : '';

$offset = ($page-1)*$rows;

$result = array();

$where = "namabarang like '%$msearchdetail%'";
$rs = mysql_query("select count(*) FROM
					orderdetail
					LEFT JOIN barang ON orderdetail.barangid = barang.idbarang
					LEFT JOIN warnabarang ON barang.warnabarangid = warnabarang.idwarnabarang
					LEFT JOIN tipebarang ON barang.tipebarangid = tipebarang.idtipebarang
					LEFT JOIN ukuranbarang ON barang.ukuranbarangid= ukuranbarang.idukuranbarang
				    INNER JOIN produksicelupdetail ON orderdetail.idtransdet = produksicelupdetail.orderdetailid where " . $where);
$row = mysql_fetch_row($rs);
$result["total"] = $row[0];

$rs = mysql_query("SELECT
					orderdetail.idtransdet,
					orderdetail.transid,
					orderdetail.barangid,
					orderdetail.satuan,
					produksicelupdetail.jumlah,
					barang.kodebarang,
					barang.namabarang, 
					barang.tipebarangid,
					tipebarang.namatipebarang,
					barang.warnabarangid,
					warnabarang.namawarnabarang,
					ukuranbarang.namaukuranbarang,
					barang.ukuranbarangid,
					DATE_FORMAT(`orderdetail`.jadwalcelup,'%m/%d/%Y') AS jadwalcelup
					FROM
					orderdetail
					LEFT JOIN barang ON orderdetail.barangid = barang.idbarang
					LEFT JOIN warnabarang ON barang.warnabarangid = warnabarang.idwarnabarang
					LEFT JOIN tipebarang ON barang.tipebarangid = tipebarang.idtipebarang
					LEFT JOIN ukuranbarang ON barang.ukuranbarangid= ukuranbarang.idukuranbarang
				    INNER JOIN produksicelupdetail ON orderdetail.idtransdet = produksicelupdetail.orderdetailid
					WHERE produksicelupdetail.transid=".$key." AND " . $where . " limit $offset,$rows");
$items = array();
while($row = mysql_fetch_object($rs)){
	array_push($items, $row);
}
$result["rows"] = $items;

echo json_encode($result);
?>