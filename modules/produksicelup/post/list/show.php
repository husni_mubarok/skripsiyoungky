<?php
	mb_internal_encoding('UTF-8');	
	$lib='../../../../lib/';
	$table="produksicelup";
	$descstatus = "CASE WHEN ".$table.".status = 0 THEN 'Open' ELSE CASE WHEN ".$table.".status = 1 THEN 'Validate' ELSE CASE WHEN ".$table.".status = 9 THEN 'Cancel' END END END";
	$colorstatus = "CASE WHEN ".$table.".status = 0 THEN 'orange' ELSE CASE WHEN ".$table.".status = 1 THEN 'green' ELSE CASE WHEN ".$table.".status = 9 THEN 'red' END END END";
	$aColumns = array( 'idtrans','notrans','tgltrans','keteranganh','colorstatus','descstatus'); 
	$sIndexColumn = 'idtrans';
	$sTable = '(SELECT
				produksicelup.idtrans,
				produksicelup.kodetrans,
				produksicelup.notrans,
				DATE_FORMAT(produksicelup.tgltrans,"%d/%m/%Y") AS tgltrans,
				produksicelup.keterangan AS keteranganh, 
				'.$colorstatus.' AS colorstatus,
				'.$descstatus.' AS descstatus
				FROM
				produksicelup) DERIVEDTBL'; 
	include_once ''.$lib.'Database.php';
	include_once ''.$lib.'showCore.php';
	while ( $aRow = $rResult->fetch_assoc() ) {
		$row = array();
		$status= '<span class="badge bg-'.$aRow['colorstatus'].'">'.$aRow['descstatus'].'</span>';
		$btn = '<a href="index.php?mod=produksicelup_ldetail&key='.$aRow['idtrans'].'">'.$aRow['notrans'].'</a>';
		for ( $i=0 ; $i<$iColumnCount ; $i++ ) {
			$row[] = $aRow[ $aColumns[$i] ];
		}
		$row = array($btn, $aRow['tgltrans'], $aRow['keteranganh'], $status);
		$output['aaData'][] = $row;
	}
	echo json_encode( $output );

?>

