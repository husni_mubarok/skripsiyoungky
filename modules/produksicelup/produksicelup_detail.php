<?php
$db=new Database();
$subnavmenu = new Subnavmenu();
$moddetail=$_GET['mod'];
$modulename=str_replace("_detail","", $moddetail);
$datasubnavmenu = $subnavmenu->showsubnavmenu($modulename);
extract($datasubnavmenu);

${"$modulename"} = new Purchaseorder();
$slip = new Slip();
	if (isset($_GET['key'])) {
	    $key = $_GET['key'];
	    $data = ${"$modulename"}->showDetail($key);
	    extract($data);
		$submodule= str_replace("modules/","", $modulesname );
		$dataslip = $slip->showSlip($submodule);
	    extract($dataslip);
	}
	if(isset($_REQUEST['saveclose'])){
        extract($_REQUEST);
		$key = $_GET['key'];
		
		
		
        ${"$modulename"}->saveclose($key, $datetrans, $supplierid, $paytypeid, $nofaktur,  $top, $remark, $datefaktur, $noppnflag);
	}
	if(isset($_REQUEST['cancel'])){
        extract($_REQUEST);
		$key = $_GET['key'];
        ${"$modulename"}->cancel($key);
	}
	if(isset($_REQUEST['delete'])){
        extract($_REQUEST);
		$key = $_GET['key'];
        ${"$modulename"}->delete($key);
	}
$key = $_GET['key'];
?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
<!-- Content Header (Page header) -->
<section class="content-header" style="margin-top:-15px !important; margin-bottom:-20px !important">
  <h4>
    <?php echo $subnavmenuname; ?>
  </h4>
  <ol class="breadcrumb">
    <li><a href="index.php"><i class="fa fa-dashboard"></i> Home</a></li>
    <li><a href="#"><?php echo $navmenuname; ?></a></li>
    <li class="active"><?php echo $subnavmenuname; ?></li>
  </ol>
</section>
<!-- Main content -->
<section class="content" onclick="generateppn();">
  <div class="row">
   
    <div class="col-xs-12">
        <div class="box box-danger">
        <form class="form-horizontal" id="formData" name="formData"  method="post" action="" enctype="multipart/form-data">
        <input type="hidden" class="form-control" id="idtrans" name="idtrans" value="<?php echo $key; ?>">
        <input type="hidden" class="form-control" id="type" name="type" value="generate">
            <div class="box-header with-border">
               <div class="row">
                <!-- Coloumn 1-->
               <div class="col-md-4">
                    <div class="form-group">
                        <label for="real_name" class="col-sm-3 control-label">Trans No</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" id="transno" name="transno"  disabled="disabled" value="<?php echo $notrans; ?>">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="real_name" class="col-sm-3 control-label">Trans Date</label>
                        <div class="col-sm-9">
                            <div class="input-group">
                               <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                              <input type="text" class="form-control pull-right" id="datetrans"  placeholder="Transaction Date" value="<?php echo $datetrans1; ?>"  name="datetrans" data-toggle="tooltip" data-placement="transdate" title="Transaction Date" data-date-format="dd/mm/yyyy">
                            </div><!-- /.input group -->
                        </div>
                    </div>
                 </div>
                 <!-- Coloumn 2-->               
                <div class="col-md-4" style="margin-bottom:-20px !important">
                    <div class="form-group">
                        <label for="real_name" class="col-sm-3 control-label">Supplier</label>
                        <div class="col-sm-9">
                            <select class="form-control select2"  style="width: 100%;" name="supplierid" id="supplierid"  placeholder="Customer">
                               <option value=<?php echo $supplierid; ?>><?php echo $suppliername; ?></option>
                               <?php
                                    $rl = new Combobox();
                                    $datarl = $rl->showSupplier();
                                    foreach ($datarl as $value) {
                                        extract($value);
                                        echo "<option value='".$idsupplier."'>".$suppliername."</option>";
                                    }
                                ?>
                              </select>
                        </div>
                    </div>
                   
					 <input type="hidden" id="paytypeid" name="paytypeid">
                    <div class="form-group">
                        <label for="real_name" class="col-sm-3 control-label">No Fakur</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" id="nofaktur" name="nofaktur" value="<?php echo $nofaktur; ?>">
                        </div>
                    </div>
					<div class="form-group">
                        <label for="real_name" class="col-sm-3 control-label">Tgl Faktur</label>
                        <div class="col-sm-9">
                            <div class="input-group">
                               <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                              <input type="text" class="form-control pull-right" id="datefaktur"  placeholder="Faktur Date" value="<?php echo $datefaktur; ?>"  name="datefaktur" data-toggle="tooltip" data-placement="transdate" title="Transaction Date" data-date-format="dd/mm/yyyy">
                            </div><!-- /.input group -->
                        </div>
                    </div>
				
                </div>
				
                 <!-- Coloumn 3-->                                   
                <div class="col-md-4">
					<div class="form-group">
                        <label for="real_name" class="col-sm-5 control-label">Tdk Include PPN</label>
                        <div class="col-sm-7">
                            
							 <select class="form-control"  style="width: 100%;" name="noppnflag" id="noppnflag"  onchange="generateppn();">
                               <option value=<?php echo $noppnflag; ?>><?php echo $noppnflagdesc; ?></option>
							   <option value=0>Tidak</option>
							   <option value=1>Ya</option>
                               
                              </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="real_name" class="col-sm-3 control-label">TOP</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" id="top" name="top" value="<?php echo $top; ?>">
                        </div>
                    </div>					
                    <div class="form-group">
                        <label for="real_name" class="col-sm-3 control-label">Comments</label>
                        <div class="col-sm-9">
                            <textarea class="form-control" id="remark" name="remark"><?php echo $remarkhead; ?></textarea>
                        </div>
                    </div>
    
                </div>
            </div>
          </div><!-- /.box-header -->
          <div class="box-body">
                <table id="dg" style="height:350px"
                        toolbar="#toolbar" pagination="true" idField="idtransdet"
                        rownumbers="true" fitColumns="true" singleSelect="false" showFooter="true" data-options="pagination:true, rownumbers:true,singleSelect:false,
                            autoRowHeight:false,pageSize:50">
                    <thead>
                        <tr>
                            <th field="idtransdetck" checkbox="true"></th>
                            <th data-options="field:'inventoryid', width:300, sortable:true, nowrap:true,
                                    formatter:function(value,row){
                                        return row.inventoryname;
                                    },
                                    editor:{
                                        type:'combogrid',
                                        options:{
                                            panelWidth:460,
                                            pagination: true,
                                            mode:'remote',
                                            pageSize:30,
                                            idField:'idinventory',
                                            textField:'inventoryname',
                                            url:'lib/combogrid/inventorypurchase.php?key=<?php echo $key; ?>',
                                            columns:[[
                                                {field:'inventorycode',title:'Product Code',width:80,sortable:true},
                                                {field:'inventoryname',title:'Product Name',width:200,sortable:true},
                                                {field:'salesunit',title:'Unit',width:50,sortable:true}
                                            ]],
                                             onSelect: function(index,row){
                                                 setTimeout(function(){
                                                     var opts = $('#dg').edatagrid('options');
                                                     var edunit = $('#dg').edatagrid('getEditor',{
                                                         index:opts.editIndex,
                                                         field:'unit'
                                                     });
                                                     $(edunit.target).combobox('setValue',row.salesunit);
                                                 },0);
                                             },
                                            fitColumns: true
                                        }
                                    }">Product</th>
                            <th data-options="field:'unit',width:100, sortable:true,
                                    formatter:function(value,row){
                                        return row.unit;
                                    },
                                    editor:{
                                        type:'combobox',
                                        options:{
                                            valueField:'unitname',
                                            textField:'unitname',
                                            method:'get',
                                            url:'lib/lookup/unit.php',
                                            required:true
                                        }
                                    }">Unit</th>
                            <th field="unitprice" width="100" align="right" editor="{type:'numberbox',options:{min:0,precision:0,groupSeparator:','}}" sortable="true">Unit Price</th>
                            <th field="quantity" width="100" align="right" editor="{type:'numberbox',options:{min:0,precision:0,groupSeparator:','}}">Quantity</th>
                            <th field="discount" width="70" align="right" editor="{type:'numberbox',options:{min:0,precision:0,groupSeparator:','}}">Potongan</th>               

                            <th field="amount" 
                                width="150" 
                                align="right" 
                                editor="{type:'numberbox',options:{precision:0,groupSeparator:',',decimalSeparator:'.'}}" 
                                sortable="true" 
                                editable:"false"
                                formatter:"function(value, row){
                                      return accounting.formatMoney(row.amount);
                                }">
                                Amount
                            </th>
                        </tr>
                    </thead>
                </table>
                <div id="toolbar">
                    <table cellpadding="0" cellspacing="0" style="width:100%">
                        <tr>
                             <td>
                                <a href="#" class="easyui-linkbutton" iconCls="icon-add" plain="true" onclick="javascript:$('#dg').edatagrid('addRow')">New</a>
                                <a href="#" class="easyui-linkbutton" iconCls="icon-save" plain="true" onclick="javascript:$('#dg').edatagrid('saveRow')">Accept</a>
                                <a href="#" class="easyui-linkbutton" iconCls="icon-remove" plain="true" onclick="javascript:$('#dg').edatagrid('destroyRow')">Destroy</a>
                                <a href="#" class="easyui-linkbutton" iconCls="icon-undo" plain="true" onclick="javascript:$('#dg').edatagrid('cancelRow')">Cancel</a>
                                <a href="#" class="easyui-linkbutton" iconCls="icon-reload" plain="true" onclick="refreshgrid()">Refresh</a>
                            </td>
                            <td style="text-align:right">
                                <input type="text" id="msearchdetail" style="line-height:18px;border:1px solid #ccc" onkeydown="keyCode(event)">
                                <a href="#" class="easyui-linkbutton" iconCls="icon-search" plain="true" onclick="doSearch()"></a>
                            </td>
                         </tr>
                    </table>
                </div>    
           </div><!-- /.col -->
    	</div>
        </div>
        </div>
         <div class="box-footer" style="margin-top:-40px !important">
            <button class="btn btn-primary" name="saveclose" onclick="javascript:$('#dg').edatagrid('saveRow')"><i class="fa fa-save"></i> Save</button>
            <a href="#" onclick="SavePrint(); javascript:$('#dg').edatagrid('saveRow');" data-toggle="modal" data-easein="swoopIn" data-target=".MyModals" type="button" class="btn btn-primary"> <i class="fa fa-print"></i> Save & Print</a>
            <button class="btn btn-warning" name="cancel"><i class="fa fa-save"></i> Cancel</button>
            <button class="btn btn-danger" name="delete"><i class="fa fa-save"></i> Delete</button>
            <a  href="index.php?mod=<?php echo str_replace("_detail","", $modulename ); ?>" type="button" class="btn btn-default"> <i class="fa fa-close"></i> Close</a>
        </div>
    </form>
</div>

</section><!-- /.content -->
</div>

<!-- Modal Popup -->
<div class="modal fade MyModals" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" >
    <div class="modal-dialog" style="width:420px !important;">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Select Print Out</h4>
            </div>
                <div class="modal-body">
                    <input type="hidden" class="form-control" id="idperiod" name="idperiod">
                    <input type="hidden" class="form-control" id="type" name="type">
                   <select multiple class="form-control" id="slipid" class="slipid">
                   <?php
						$i=1;
						if($dataslip !=0){
						foreach ($dataslip as $valueslip) {
						extract($valueslip);
					?>    
                    <option ondblclick="window.open('report.php?mod=<?php echo $module ?>',target='_blank')" value="<?php echo $module ?>"><?php echo $slipname ?></option>
                    <?php
						$i++;
							}
							}
					?> 
                   </select>
                </div>
            <div class="modal-footer">
            	<button type="button" class="btn btn-primary btn-flat" onclick="window.open('?mod=slip')"> <i class="fa fa-wrench"></i> Config</button>
                <button type="button" class="btn btn-primary btn-flat" onclick="showslip()"> <i class="fa fa-search"></i> Preview</button>
                <button type="button" onclick="ClearVal()" class="btn btn-default btn-flat pull-left" data-dismiss="modal"> <i class="fa fa-close"></i> Close</button>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
function generateppn()
{
	var formData = $("#formData").serialize();
	$.ajax({
		type: "POST",
		url: "<?php echo $pagepost;?>generateppn.php",
		dataType: 'json',
		data: formData,
		success: function(data) {
			//$("#programid").val(data.programid);
			$("#type").val("generate");
			//alert("test");
		}
	});

//alert("test");
}





var key = "<?php echo $key; ?>";

$(function(){
   showdetail();
});
</script>
<?php
	echo "<link rel='stylesheet' type='text/css' href='modules/".$modulename."/css/page.css' />";
	echo "<script type='text/javascript' src='modules/".$modulename."/js/page.js'></script>";
?>


