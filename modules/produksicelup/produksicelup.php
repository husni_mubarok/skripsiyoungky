<?php
	$db=new Database();
	$subnavmenu = new Subnavmenu();
	$modulename=$_GET['mod'];
	
	$moduleget=str_replace("","", $moddetail);
	$datasubnavmenu = $subnavmenu->showsubnavmenu($modulename);
	extract($datasubnavmenu);
	
	${"$moduleget"} = new Produksicelup();
	extract($dataheader);
	//Show detail
	$datadetail = ${"$moduleget"}->showOpenDetail($key);
	extract($datadetail);
	
	if(isset($_REQUEST['submit'])){
		extract($_REQUEST);
		${"$moduleget"}->pickheader($kodetrans, $notrans, $tgltrans);
		${"$moduleget"}->pickdetail($idtransdet, $totalpick); 
	} 
	
?>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
<!-- Content Header (Page header) -->
<section class="content-header" style="margin-top:-15px !important; margin-bottom:-20px !important">
  <h4>
    <?php echo $subnavmenuname; ?>
  </h4>
  <ol class="breadcrumb">
    <li><a href="index.php"><i class="fa fa-dashboard"></i> Home</a></li>
    <li><a href="#"><?php echo $navmenuname; ?></a></li>
    <li class="active"><?php echo $subnavmenuname; ?></li>
  </ol>
</section>
<!-- Main content -->
<section class="content">
  <div class="row">
    <div class="col-xs-12">
     <!-- Custom Tabs -->
      <div class="nav-tabs-custom">
        <ul class="nav nav-tabs">
          <li class="active"><a href="#tab_1" data-toggle="tab" onClick="RefreshData()"><?php echo $subnavmenuname; ?> Open</a></li>
          <li><a href="#tab_2" data-toggle="tab" onClick="RefreshData()"><?php echo $subnavmenuname; ?> List</a></li>
        </ul>
        <div class="tab-content">
         <div class="tab-pane active" id="tab_1">
         <div class="box box-danger">
            <div class="box-body">
               <form id="form" name="formproduk"  class="editprofileform" method="post" action="" enctype="multipart/form-data" onsubmit="return validateForm()">
                 <input type="hidden" class="form-control" id="supplierid" name="supplierid" value="<?php echo $idsupplier; ?>">
                  <table id="example1" class="table table-bordered table-striped">
                    <thead>
                      <tr>
                        <th>No</th>
                        <th>No Order</th>
                        <th>Tgl Order</th>
                        <th>Pelanggan</th>
                        <th>Barang</th>
                        <th>Tipe</th>
                        <th>Warna</th>
                        <th>Ukuran</th>
                        <th>Schedule</th>
                        <th>Jumlah</th>
                        <th>Jumlah Sisa</th>
                        <th><input type="checkbox" name="selectall" id="selectall" value="" onchange="calculate()"/></th>
                        <th>Total Pick</th>
                      </tr>
                    </thead>
                    <tbody>
                    <?php
                        $i=1;
                        if($datadetail !=0){
                        foreach ($datadetail as $valuepurorder) {
                        extract($valuepurorder);
                    ?>    
                    <tr>
                        <td><?php echo $i; ?></td>
                        <td class="left"><?php echo $notrans; ?></td>
                        <td class="left"><?php echo $tgltrans; ?></td>
                        <td class="left"><?php echo $namapelanggan; ?></td>
                        <td class="left"><?php echo $namabarang; ?></td>
                        <td class="left"><?php echo $namatipebarang; ?></td>
                        <td class="left"><?php echo $namawarnabarang; ?></td>
                        <td class="left"><?php echo $namaukuranbarang; ?></td> 
                        <td class="left"><?php echo $jadwalcelup; ?></td> 
                        <td class="left"><?php echo number_format($jumlah); ?></td>
                        <td class="left"><?php echo number_format($jumlahsisa); ?></td>
                        <td class="centeralign">
                            <input type="checkbox" name="checkedid[]" class="checkbox" id="check" value="<?PHP echo $idtransdet;?>" onchange="myFunction(this)">
                        </td>
                        <td>
                            <input type="text" class="form-control input-sm" style="height:22px; margin-bottom:-3px; margin-top:-4px !important" name="totalpick[]" id="totalpick<?PHP echo $idtransdet;?>" value="<?PHP echo $jumlahsisa;?>" disabled="disabled"  onkeyup="numericFilter(this); updateSum();  valpick<?PHP echo $idtransdet;?>();" >
                        </td>
                    </tr>
                    
                    <input type="text" style="width:1px !important; height:1px !important; border-color:#FFFFFF !important; border-style:none" id="totalpickc<?PHP echo $idtransdet;?>" value="<?PHP echo $poremain;?>">
					<!-- <script type="text/javascript">
                        function valpick<?PHP echo $idtransdet;?>(){
                            var q1<?PHP echo $idtransdet;?> = +document.getElementById("totalpickc<?PHP echo $idtransdet;?>").value;
                            var q2<?PHP echo $idtransdet;?> = +document.getElementById("totalpick<?PHP echo $idtransdet;?>").value;
                            if (q1<?PHP echo $idtransdet;?> < q2<?PHP echo $idtransdet;?>){
                                alert ("Total pick melebihi remain quantity!");
								
                                document.getElementById("totalpick<?PHP echo $idtransdet;?>").value = document.getElementById("totalpickc<?PHP echo $idtransdet;?>").value;
								updateSum();
                            }
                        };
                    </script> -->
                    
                    <?php
                        $i++;
                            }
                            } 
                    ?>   
                    </tbody> 
                    <tfoot>
                      <tr>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th>Total</th>
                        <th ><div id="sum"></div></th>
                      </tr>
                    </tfoot>
                  </table>
                  <a href="#" data-toggle="modal" data-easein="swoopIn" data-target=".MyModals" type="button"  class="btn btn-primary  btn-flat pull-right" onclick="showModalsPick()"><i class="fa fa-check"></i> Pick</a>

                    <!-- Modal Popup -->
                    <div class="modal fade MyModals" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" >
                        <div class="modal-dialog" style="width:420px !important;">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                    <h4 class="modal-title" id="myModalLabel">Pilih kode transaksi</h4>
                                </div>
                                    <div class="modal-body">
                                        <input type="hidden" class="form-control" id="idperiod" name="idperiod">
                                        <input type="hidden" class="form-control" id="type" name="type">
                                        <div class="form-group">
                                            <label for="real_name" class="col-sm-3 control-label">Transaksi</label>
                                            <div class="col-sm-8">
                                              <select class="form-control"  style="width: 100%;" name="kodetrans" id="kodetrans"  placeholder="Kode Transaksi">
                                                  <option value="PRCL">Produksi Celup Benang</option>
                                              </select>
                                            </div>
                                        </div>
                                    </div>
                                <div class="modal-footer">
                                    <button class="btn btn-warning btn-flat">Submit</button>
                                    <input type="hidden" value="1" name="submit" />
                                    <button type="button" onclick="ClearVal()" class="btn btn-default btn-flat pull-left" data-dismiss="modal">Tutup</button>
                                </div>
                                </form>
                            </div>
                        </div>
                    </div>
        	</div><!-- /.box-body -->
     	</div>
     </div><!-- /.tab-pane -->
     <div class="tab-pane" id="tab_2">
      <div class="box box-danger">
      <div class="box-header with-border">
          <button onClick="history.back()" type="button" class="btn btn-primary btn-flat" data-toggle="tooltip" data-placement="top" > <i class="fa fa-undo"></i> Kembali</button>
          <button onClick="RefreshData()" type="button" class="btn btn-success btn-flat"> <i class="fa fa-refresh"></i> Refresh </button>
          <div class="box-tools pull-right">
              <div class="tableTools-container">
                  <button class="btn btn-white btn-primary  btn-bold" data-widget="collapse"><i class="fa fa-minus"></i></button>
              </div>
          </div><!-- /.box-tools -->
      </div>
      <div class="box-header" style="margin-top:-20px">
        </div><!-- /.box-header -->
        <div class="box-body">
        <table id="datagridlist"  class="table table-bordered table-hover">
            <thead>
                <tr>
                    <th>No Produksi</th>
                    <th>TglPproduksi</th> 
                    <th>Keterangan</th>
                    <th>Status</th>
                </tr>
            </thead>
            <tbody>
            </tbody>
        </table>		
        </div><!-- /.box-body -->
      </div><!-- /.box -->
      </div><!-- /.tab-pane -->
  </div>
  </div>
    </div><!-- /.col -->
  </div><!-- /.row --> 
</section><!-- /.content -->
</div>

<?php
	echo "<link rel='stylesheet' type='text/css' href='modules/".$modulename."/css/open/page.css' />";
	echo "<script type='text/javascript' src='modules/".$modulename."/js/open/page.js'></script>";
	echo "<link rel='stylesheet' type='text/css' href='modules/".$modulename."/css/list/page.css' />";
	echo "<script type='text/javascript' src='modules/".$modulename."/js/list/page.js'></script>";
?>