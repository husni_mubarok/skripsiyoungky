<?php
	$user = $_GET['user'];
	mb_internal_encoding('UTF-8');	
	$lib='../../../lib/';
	$aColumns = array( 'idkey','kodetrans','notrans','tgltrans','jatuhtempo','matauang','namapemasok','dibayar'); 
	$sIndexColumn = 'idkey';
	$sTable = '(SELECT
					hutang.idtrans AS idkey,
					hutang.kodetrans,
					hutang.notrans,
					DATE_FORMAT(hutang.tgltrans, "%d/%m/%Y") AS tgltrans,
					DATE_FORMAT(
						hutang.jatuhtempo,
						"%d/%m/%Y"
					) AS jatuhtempo,
					hutang.matauang,
					hutang.pemasokid,
					pemasok.namapemasok,
					FORMAT(hutang.jumlah, 0) AS jumlah,
					hutang.keterangan,
					hutang. STATUS,
					FORMAT(
						IFNULL(bayarhutang.jumlahbayar, 0),
						0
					) AS dibayar,
					FORMAT(
						hutang.jumlah - IFNULL(bayarhutang.jumlahbayar, 0),
						0
					) AS sisa,
					CASE
				WHEN hutang.status = 0 THEN
					"green"
				ELSE
					"red"
				END AS colorstatus,
				 CASE
				WHEN hutang. STATUS = 0 THEN
					"Validasi"
				ELSE
					"Batal"
				END AS descstatus
				FROM
					hutang
				LEFT JOIN pemasok ON hutang.pemasokid = pemasok.idpemasok
				LEFT JOIN (
					SELECT
						hutangid,
						SUM(jumlah) AS jumlahbayar
					FROM
						bayarhutangdet
					GROUP BY
						hutangid
				) AS bayarhutang ON hutang.idtrans = bayarhutang.hutangid,
								users
								WHERE users.username="'.$user.'" AND hutang.tgltrans BETWEEN users.begindate AND users.enddate
				AND
					hutang.jumlah - IFNULL(bayarhutang.jumlahbayar, 0) = 0) DERIVEDTBL'; 
	include_once ''.$lib.'Database.php';
	include_once ''.$lib.'showCore.php';
	while ( $aRow = $rResult->fetch_assoc() ) {
		$row = array();
		for ( $i=0 ; $i<$iColumnCount ; $i++ ) {
			$row[] = $aRow[ $aColumns[$i] ];
		}

		$row = array( $aRow['notrans'], $aRow['tgltrans'], $aRow['jatuhtempo'], $aRow['matauang'], $aRow['namapemasok'], $aRow['dibayar']);
		$output['aaData'][] = $row;
	}
	echo json_encode( $output );

?>
