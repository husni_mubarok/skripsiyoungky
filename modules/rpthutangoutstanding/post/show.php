<?php
	$user = $_GET['user'];
	mb_internal_encoding('UTF-8');	
	$lib='../../../lib/';
	$aColumns = array( 'idkey','notrans','jumlah','dibayar','sisa'); 
	$sIndexColumn = 'idkey';
	$sTable = '(SELECT
					hutang.idtrans AS idkey,
					hutang.kodetrans,
					hutang.notrans,   
					hutang.pemasokid,
					pemasok.namapemasok,
					FORMAT(hutang.jumlah, 0) AS jumlah,
					hutang.keterangan, 
					FORMAT(
						IFNULL(bayarhutang.jumlahbayar, 0),
						0
					) AS dibayar,
					FORMAT(
						hutang.jumlah - IFNULL(bayarhutang.jumlahbayar, 0),
						0
					) AS sisa
				FROM
					hutang
				LEFT JOIN pemasok ON hutang.pemasokid = pemasok.idpemasok
				LEFT JOIN (
					SELECT
						hutangid,
						SUM(jumlah) AS jumlahbayar
					FROM
						bayarhutangdet
					GROUP BY
						hutangid
				) AS bayarhutang ON hutang.idtrans = bayarhutang.hutangid,
				 users
				WHERE
					users.username = "'.$user.'"
				AND hutang.tgltrans BETWEEN users.begindate
				AND users.enddate
				AND hutang.jumlah - IFNULL(bayarhutang.jumlahbayar, 0) > 0) DERIVEDTBL'; 
	include_once ''.$lib.'Database.php';
	include_once ''.$lib.'showCore.php';
	while ( $aRow = $rResult->fetch_assoc() ) {
		$row = array();
		for ( $i=0 ; $i<$iColumnCount ; $i++ ) {
			$row[] = $aRow[ $aColumns[$i] ];
		}

		$row = array( $aRow['notrans'], $aRow['jumlah'], $aRow['dibayar'], $aRow['sisa']);
		$output['aaData'][] = $row;
	}
	echo json_encode( $output );

?>
