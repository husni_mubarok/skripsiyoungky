 <style>
  .example-modal .modal {
    position: relative;
    top: auto;
    bottom: auto;
    right: auto;
    left: auto;
    display: block;
    z-index: 1;
  }
  .example-modal .modal {
    background: transparent !important;
  }
</style>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    Registration Successful
  </h1>
  <ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
    <li><a href="#">Utility</a></li>
    <li class="active">Registration Successful</li>
  </ol>
</section>
  <div class="example-modal">
    <div class="modal modal-success">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title">Registration Successful</h4>
          </div>
          <div class="modal-body">
            <p>Successful registration, you can log in with a username that is already registered</p>
          </div>
          <div class="modal-footer">
            <a href="?mod=adduser" class="btn btn-outline pull-left"><b>Register of Other Users</b></a>
            <a href="logout.php" class="btn btn-outline"><b>Login</b></a>
          </div>
        </div><!-- /.modal-content -->
      </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
  </div><!-- /.example-modal -->
  </div><!-- /.example-modal -->
</section><!-- /.content -->
</div><!-- /.content-wrapper -->