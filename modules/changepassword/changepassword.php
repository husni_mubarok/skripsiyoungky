<?php
$db=new Database();
$users = new Users();
    if(isset($_REQUEST['savedate'])){
	extract($_REQUEST);
		$users->changepassword($password);
	}
?>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
<!-- Content Header (Page header) -->


<!-- Main content -->
<section class="content">
  <div class="row">
    <div class="col-xs-12">
<!-- Content Header (Page header) -->
<body class="hold-transition register-page">
    <div class="register-box">
      <div class="register-logo">
        <a href="index.php"><b>Ganti</b>Password</a>
      </div>

      <div class="register-box-body">
        <p class="login-box-msg">Change the default password</p>
        <form id="defaultForm" method="post" action="" enctype="multipart/form-data">
          <div class="form-group has-feedback">
            <input type="text" class="form-control" placeholder="ID Pengguna" value="<?php echo $_SESSION['uname'] ?>" disabled="disabled">
            <span class="glyphicon glyphicon-user form-control-feedback"></span>
          </div>
          <div class="form-group has-feedback">
             <input type="password" class="form-control" id="password" name="password" placeholder="Type password">
            <span class="glyphicon glyphicon-lock form-control-feedback"></span>
          </div>
          <div class="form-group has-feedback">
            <input type="password" class="form-control" placeholder="Retype password" name="confirmPassword" id="confirmPassword">
          </div>
          <div class="row">
            <div class="col-xs-6">
              <button type="submit" class="btn btn-primary btn-block btn-flat" value="1" name="savedate" >Change Password</button>
            </div><!-- /.col -->
           </div>
        </form>
      </div><!-- /.form-box -->
    </div><!-- /.register-box -->
 </body>
 </div>
 </div>
 </section>
 </div>

<script type="text/javascript">
$(document).ready(function() {
    $('#defaultForm').formValidation({
       err: {
            container: 'tooltip'
        },
        icon: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            password: {
                validators: {
                    notEmpty: {
                        message: 'The password is required and can\'t be empty'
                    }
                }
            },
            confirmPassword: {
                validators: {
                    notEmpty: {
                        message: 'The confirm password is required and can\'t be empty'
                    },
                    identical: {
                        field: 'password',
                        message: 'The password and its confirm are not the same'
                    }
                }
            }
        }
    });
});
</script>