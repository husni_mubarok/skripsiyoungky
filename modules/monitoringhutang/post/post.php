<?php
$lib='../../../lib/';
include_once ''.$lib.'Database.php';
switch ($_POST['type']) {
	case "get":
		$SQL = mysqli_query($con, "SELECT
				hutang.idtrans AS idkey,
				hutang.kodetrans,
				hutang.notrans,
				DATE_FORMAT(hutang.tgltrans,'%m/%d/%Y') AS tgltrans,
				DATE_FORMAT(hutang.jatuhtempo,'%m/%d/%Y') AS jatuhtempo,
				hutang.pemasokid,
				hutang.matauang,
				pemasok.namapemasok,
				FORMAT(hutang.jumlah,0) AS jumlah,
				hutang.keterangan,
				hutang.status
				FROM
				hutang
				LEFT JOIN pemasok ON hutang.pemasokid = pemasok.idpemasok
				WHERE hutang.idtrans='".$_POST['idkey']."'");
		$return = mysqli_fetch_array($SQL,MYSQLI_ASSOC);
		echo json_encode($return);
		break;
	case "new":

		$varjatuhtempo = $_POST['jatuhtempo'];
		$jatuhtempo = date('Y-m-d', strtotime($varjatuhtempo));
		$SQL = mysqli_query($con, 
		"INSERT INTO hutang (kodetrans, notrans, tgltrans)
				SELECT 'CSPY',
				IFNULL(CONCAT('CSPY','-',DATE_FORMAT(NOW(), '%d%m%y'),RIGHT((RIGHT(MAX(hutang.notrans),3))+1001,3)), CONCAT('CSPY','-',DATE_FORMAT(NOW(), '%d%m%y'),'001')), DATE(NOW())  
				FROM
				hutang
				WHERE kodetrans='CSPY'");
		if($SQL){
			echo json_encode("OK");
		}
		
		$SQL1 = mysqli_query($con, 
		"UPDATE hutang A INNER JOIN (SELECT
		MAX(hutang.idtrans) AS idtrans
		FROM
		hutang) U SET pemasokid='".$_POST['pemasokid']."', jumlah='".$_POST['jumlah']."', keterangan='".$_POST['keterangan']."', jatuhtempo='".$jatuhtempo."', matauang='".$_POST['matauang']."'  WHERE A.idtrans=U.idtrans");
		if($SQL1){
			echo json_encode("OK");
		}
		
		break;
		
	case "edit":
		$vardatetime = $_POST['tgltrans'];
		$tgltrans = date('Y-m-d', strtotime($vardatetime));

		$varjatuhtempo = $_POST['jatuhtempo'];
		$jatuhtempo = date('Y-m-d', strtotime($varjatuhtempo));
		
		$SQL = mysqli_query($con, 
		"UPDATE hutang SET tgltrans='".$tgltrans."', pemasokid='".$_POST['pemasokid']."', jumlah='".$_POST['jumlah']."', keterangan='".$_POST['keterangan']."', jatuhtempo='".$jatuhtempo."', matauang='".$_POST['matauang']."'  WHERE idtrans='".$_POST['idkey']."'");
		if($SQL){
			echo json_encode("OK");
		}			
		break;
	case "delete":
		$SQL = mysqli_query($con, "DELETE FROM hutang WHERE idtrans='".$_POST['idkey']."'");
		if($SQL){
			echo json_encode("OK");
		}			
		break;
} 
?>
