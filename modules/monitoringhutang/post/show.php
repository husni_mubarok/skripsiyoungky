<?php
	mb_internal_encoding('UTF-8');	
	$lib='../../../lib/';
	$aColumns = array( 'idkey','notrans','tgltrans','namapemasok','jumlah','jatuhtempo','matauang','sisa', 'dibayar','colorstatus','descstatus'); 
	$sIndexColumn = 'idkey';
	$sTable = '(SELECT
				hutang.idtrans AS idkey,
				hutang.kodetrans,
				hutang.notrans,
				DATE_FORMAT(hutang.tgltrans,"%d/%m/%Y") AS tgltrans,
				DATE_FORMAT(hutang.jatuhtempo,"%d/%m/%Y") AS jatuhtempo,
				hutang.matauang,
				hutang.pemasokid,
				pemasok.namapemasok,
				FORMAT(hutang.jumlah,0) AS jumlah,
				hutang.keterangan,
				hutang.status,
				FORMAT(IFNULL(bayarhutang.jumlahbayar,0),0) AS dibayar,
				FORMAT(hutang.jumlah - IFNULL(bayarhutang.jumlahbayar,0),0) AS sisa,
				CASE WHEN hutang.status = 0 THEN "green" ELSE "red" END AS colorstatus,
				CASE WHEN hutang.status = 0 THEN "Validasi" ELSE "Batal" END AS descstatus
				FROM
				hutang
				LEFT JOIN pemasok ON hutang.pemasokid = pemasok.idpemasok
				LEFT JOIN (SELECT hutangid, SUM(jumlah) AS jumlahbayar FROM bayarhutangdet GROUP BY hutangid) AS bayarhutang ON hutang.idtrans = bayarhutang.hutangid) DERIVEDTBL'; 
	include_once ''.$lib.'Database.php';
	include_once ''.$lib.'showCore.php';
	while ( $aRow = $rResult->fetch_assoc() ) {
		$row = array();
		$status= '<span class="badge bg-'.$aRow['colorstatus'].'">'.$aRow['descstatus'].'</span>'; 
		for ( $i=0 ; $i<$iColumnCount ; $i++ ) {
			$row[] = $aRow[ $aColumns[$i] ];
		}
		$row = array( $aRow['notrans'], $aRow['tgltrans'], $aRow['namapemasok'], $aRow['jatuhtempo'], $aRow['matauang'], $aRow['jumlah'], $aRow['dibayar'], $aRow['sisa'],$status);
		$output['aaData'][] = $row;
	}
	echo json_encode( $output );

?>

