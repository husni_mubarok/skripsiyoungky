<?php
	mb_internal_encoding('UTF-8');	
	$lib='../../../lib/';
	$descstatus = "CASE WHEN order.status = 9 THEN 'Batal' ELSE 'Aktif' END";
	$colorstatus = "CASE WHEN order.status = 9 THEN 'orange' ELSE 'green' END";
	$aColumns = array( 'idtrans','notrans','tgltrans','namapelanggan','keterangan','colorstatus','descstatus'); 
	$sIndexColumn = 'idtrans';
	$sTable = '(SELECT
				`order`.idtrans,
				`order`.kodetrans,
				`order`.notrans,
				DATE_FORMAT(order.tgltrans,"%d/%m/%Y") AS tgltrans,
				`order`.keterangan,
				`order`.pelangganid,
				pelanggan.namapelanggan,
				'.$colorstatus.' AS colorstatus,
				'.$descstatus.' AS descstatus
				FROM
				`order`
				LEFT JOIN pelanggan ON `order`.pelangganid = pelanggan.idpelanggan) DERIVEDTBL'; 
	include_once ''.$lib.'Database.php';
	include_once ''.$lib.'showCore.php';
	while ( $aRow = $rResult->fetch_assoc() ) {
		$row = array();
		$status= '<span class="badge bg-'.$aRow['colorstatus'].'">'.$aRow['descstatus'].'</span>';
		$btn = '<a href="?mod=scheduleorder_detail&key='.$aRow['idtrans'].'")">'.$aRow['notrans'].'</a>';
		for ( $i=0 ; $i<$iColumnCount ; $i++ ) {
			$row[] = $aRow[ $aColumns[$i] ];
		}
		$row = array($btn, $aRow['tgltrans'], $aRow['namapelanggan'], $aRow['keterangan'], $status);
		$output['aaData'][] = $row;
	}
	echo json_encode( $output );

?>

