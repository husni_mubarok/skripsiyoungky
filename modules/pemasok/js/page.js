//Declare string variable
var modulename = "pemasok";
var moduledesc = "Pemasok";

//Show function
var dTable;
$(document).ready(function() {
	dTable = $('#datagrid').DataTable( {
		"bProcessing": true,
		"bServerSide": true,
		"bJQueryUI": false,
		"responsive": true,
		"pageLength": 50,
		"scrollY": "360px",
		"order": [[ 1, "asc" ]],
		"lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
		"sAjaxSource": "modules/" + modulename + "/post/show.php",
		"sServerMethod": "POST",
		"columnDefs": [
		{ "orderable": false, "targets": 0, "searchable": false },
		{ "orderable": true, "targets": 1, "searchable": true },
		{ "orderable": true, "targets": 2, "searchable": true },
		{ "orderable": true, "targets": 3, "searchable": true },
		{ "orderable": true, "targets": 4, "searchable": true },
		{ "orderable": true, "targets": 5, "searchable": true },
		{ "orderable": true, "targets": 6, "searchable": true }
		]
	} );
	$('#datagrid').removeClass( 'display' ).addClass('table table-striped table-bordered');
	$('#datagrid tfoot th').each( function () {
		if( $(this).text() != "Action" ){
			var title = $('#datagrid thead th').eq( $(this).index() ).text();
			$(this).html( '<input type="text" placeholder="Search '+title+'" class="form-control" />' );
		}
	} );
	
} );

//Post function
function showModals( idkey )
{
	clearModals();
	document.getElementById("submitnew").style.display = "block"; 
	document.getElementById("submitaddnew").style.display = "block"; 
	document.getElementById("submitdelete").style.display = "none";
	if( idkey )
	{
		clearModals();
		$.ajax({
			type: "POST",
			url: "modules/" + modulename + "/post/post.php",
			dataType: 'json',
			data: {idkey:idkey,type:"get"},
			success: function(res) {
				waitingDialog.hide();
				setModalData( res );
			}
		});
	}
	else
	{
		$("#myModalLabel").html("Tambah " + moduledesc );
		$("#type").val("new"); 
		waitingDialog.hide();
		$("#myModals").modal("show");
	}
}

function setModalData( data )
{
	clearModals();
	$("#myModalLabel").html("Edit " + moduledesc);
	$("#myModalLabel").html(data.real_name);
	$("#idkey").val(data.idkey);
	$("#type").val("edit");
	$("#noregistrasi").val(data.noregistrasi);
	$("#tglregistrasi").val(data.tglregistrasi);
	$("#namapemasok").val(data.namapemasok);
	$("#alamat1").val(data.alamat1);
	$("#alamat2").val(data.alamat2);
	$("#kotaid").val(data.kotaid);
	$("#kodepos").val(data.kodepos);
	$("#tlp1").val(data.tlp1);
	$("#tlp2").val(data.tlp2);
	$("#fax").val(data.fax);
	$("#email").val(data.email);
	$("#tipepemasokid").val(data.tipepemasokid);
	$("#bankid").val(data.bankid);
	$("#norekening").val(data.norekening);
	var status = data.status;
	$('#status option[value="' + status +'"]').prop("selected", true);
	$("#myModals").modal("show");
	waitingDialog.hide();	
}

function deleteData( idkey )
{
	clearModals();
	$.ajax({
		type: "POST",
		url: "modules/" + modulename + "/post/post.php",
		dataType: 'json',
		data: {idkey:idkey,type:"get"},
		success: function(data) {
			$("#myModalLabel").html("Hapus " + moduledesc);
			$("#idkey").val(data.idkey);
			$("#noregistrasi").val(data.noregistrasi);
			$("#tglregistrasi").val(data.tglregistrasi);
			$("#namapemasok").val(data.namapemasok);
			$("#alamat1").val(data.alamat1);
			$("#alamat2").val(data.alamat2);
			$("#kotaid").val(data.kotaid);
			$("#kodepos").val(data.kodepos);
			$("#tlp1").val(data.tlp1);
			$("#tlp2").val(data.tlp2);
			$("#fax").val(data.fax);
			$("#email").val(data.email);
			$("#tipepemasokid").val(data.tipepemasokid);
			$("#bankid").val(data.bankid); 
			$("#norekening").val(data.norekening); 
			$("#type").val("delete");
			$("#noregistrasi").val(data.noregistrasi).attr("disabled","false");
			$("#tglregistrasi").val(data.tglregistrasi).attr("disabled","false");
			$("#namapemasok").val(data.namapemasok).attr("disabled","false");
			$("#alamat1").val(data.alamat1).attr("disabled","false");
			$("#alamat2").val(data.alamat2).attr("disabled","false");
			$("#kotaid").val(data.kotaid).attr("disabled","false");
			$("#kodepos").val(data.kodepos).attr("disabled","false");
			$("#tlp1").val(data.tlp1).attr("disabled","false");
			$("#tlp2").val(data.tlp2).attr("disabled","false");
			$("#fax").val(data.fax).attr("disabled","false");
			$("#email").val(data.email).attr("disabled","false");
			$("#tipepemasokid").val(data.tipepemasokid).attr("disabled","false");
			$("#bankid").val(data.bankid).attr("disabled","false");
			$("#norekening").val(data.norekening).attr("disabled","false");
			
			var status = data.status;
			$('#status option[value="' + status +'"]').prop("selected", true);

			var tipepemasokid = data.tipepemasokid;
			$('#tipepemasokid option[value="' + tipepemasokid +'"]').prop("selected", true);

			var bankid = data.bankid;
			$('#bankid option[value="' + bankid +'"]').prop("selected", true);

			var kotaid = data.kotaid;
			$('#kotaid option[value="' + kotaid +'"]').prop("selected", true);
	
			$("#myModals").modal("show");
			document.getElementById("submitnew").style.display = "none"; 
			document.getElementById("submitaddnew").style.display = "none"; 
			document.getElementById("submitdelete").style.display = "block";

			waitingDialog.hide();			
		}
	});
}

function submitData()
{
	var counter = document.getElementById("namapemasok").value; 
	if (counter == '') {
	  $('#formData').formValidation('validate');
	  } else {
			var formData = $("#formData").serialize();
			waitingDialog.show();
			$.ajax({
				type: "POST",
				url: "modules/" + modulename + "/post/post.php",
				dataType: 'json',
				data: formData,
				success: function(data) {
					dTable.ajax.reload();
					waitingDialog.hide();	
					$('#myModals').modal('hide');
				}
			});
	}
}
function submitDataAdd()
{
	var counter = document.getElementById("namapemasok").value; 
	if (counter == '') {
	  $('#formData').formValidation('validate');
	  } else {
			var formData = $("#formData").serialize();
			waitingDialog.show();
			$.ajax({
				type: "POST",
				url: "modules/" + modulename + "/post/post.php",
				dataType: 'json',
				data: formData,
				success: function(data) {
					dTable.ajax.reload();
					waitingDialog.hide();	
					showModals();
				}
			});
	}
}
function clearModals()
{
	$("#removeWarning").hide();
	$("#idkey").val("").removeAttr( "disabled" );
	$("#noregistrasi").val("").removeAttr( "disabled" );
	$("#tglregistrasi").val("").removeAttr( "disabled" );
	$("#namapemasok").val("").removeAttr( "disabled" );
	$("#alamat1").val("").removeAttr( "disabled" );
	$("#alamat2").val("").removeAttr( "disabled" );
	$("#kotaid").val("").removeAttr( "disabled" );
	$("#kodepos").val("").removeAttr( "disabled" );
	$("#tlp1").val("").removeAttr( "disabled" );
	$("#tlp2").val("").removeAttr( "disabled" );
	$("#fax").val("").removeAttr( "disabled" );
	$("#email").val("").removeAttr( "disabled" );
	$("#tipepemasokid").val("").removeAttr( "disabled" );
	$("#bankid").val("").removeAttr( "disabled" );
	$("#norekening").val("").removeAttr( "disabled" );
	$("#type").val("");
}

function RefreshData()
{
	dTable.ajax.reload();
}

//Validation function
$(document).ready(function() {
    $('#formData').formValidation({
		framework: 'bootstrap',
        excluded: ':disabled',
        err: {
            container: 'tooltip'
        },
        icon: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            namapemasok: {
                validators: {
                    stringLength: {
                        enabled: true,
                        min: 1,
                        message: ''
                    },
                    notEmpty: {
                        message: 'Pemasok name is required'
                    }
                }
            }
        }
    })
	 .on('success.form.fv', function(e) {
		var $form     = $(e.target),
			validator = $form.data('formValidation');
		$form
			.formValidation('resetForm', true);    
	});
});
$('#myModals').on('hidden.bs.modal', function() {
    $('#formData').formValidation('resetForm', true);
});
