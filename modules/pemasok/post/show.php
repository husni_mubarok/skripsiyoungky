<?php
	mb_internal_encoding('UTF-8');	
	$lib='../../../lib/';
	$aColumns = array( 'idkey', 'kode', 'namapemasok','alamat1','kodepos','tlp1','fax','email','namabank','norekening','colorstatus','descstatus'); 
	$sIndexColumn = 'idkey';
	$sTable = '(SELECT
				pemasok.idpemasok AS idkey,
				CONCAT(LEFT(UPPER(pemasok.namapemasok),2), "/", LPAD(pemasok.idpemasok, 4, "0")) AS kode,
				pemasok.noregistrasi,
				pemasok.tglregistrasi,
				pemasok.namapemasok,
				pemasok.alamat1,
				pemasok.alamat2,
				pemasok.kodepos,
				pemasok.tlp1,
				pemasok.tlp2,
				pemasok.fax,
				pemasok.email,
				pemasok.kotaid,
				pemasok.tipepemasokid,
				pemasok.status,
				pemasok.keterangan,
				pemasok.bankid,
				bank.namabank,
				pemasok.norekening,
				CASE WHEN pemasok.status = 0 THEN "green" ELSE "red" END AS colorstatus,
				CASE WHEN pemasok.status = 0 THEN "Aktif" ELSE "Tidak Aktif" END AS descstatus
				FROM
				pemasok
				LEFT JOIN bank ON pemasok.bankid = bank.idbank
				ORDER BY pemasok.namapemasok ASC) DERIVEDTBL'; 
	include_once ''.$lib.'Database.php';
	include_once ''.$lib.'showCore.php';
	while ( $aRow = $rResult->fetch_assoc() ) {
		$row = array();
		$status= '<span class="badge bg-'.$aRow['colorstatus'].'">'.$aRow['descstatus'].'</span>';
		$btn = '<a href="#" onClick="showModals(\''.$aRow['idkey'].'\')">Edit</a> | <a href="#" onClick="deleteData(\''.$aRow['idkey'].'\')">Hapus</a>';
		for ( $i=0 ; $i<$iColumnCount ; $i++ ) {
			$row[] = $aRow[ $aColumns[$i] ];
		}
		$row = array( $btn, $aRow['kode'], $aRow['namapemasok'], $aRow['alamat1'], $aRow['tlp1'],  $aRow['namabank'], $aRow['norekening'], $status);
		$output['aaData'][] = $row;
	}
	echo json_encode( $output );

?>

