<?php
	mb_internal_encoding('UTF-8');	
	$lib='../../../lib/';
	$aColumns = array( 'idkey','namakota','colorstatus','descstatus', 'kode'); 
	$sIndexColumn = 'idkey';
	$sTable = '(SELECT
				kota.idkota AS idkey,
				CONCAT(LEFT(UPPER(kota.namakota),2), "-", LPAD(kota.idkota, 2, "0")) AS kode,
				kota.namakota,
				kota.status,
				CASE WHEN kota.status = 0 THEN "green" ELSE "red" END AS colorstatus,
				CASE WHEN kota.status = 0 THEN "Aktif" ELSE "Tidak Aktif" END AS descstatus
				FROM
				kota) DERIVEDTBL'; 
	include_once ''.$lib.'Database.php';
	include_once ''.$lib.'showCore.php';
	while ( $aRow = $rResult->fetch_assoc() ) {
		$row = array();
		$status= '<span class="badge bg-'.$aRow['colorstatus'].'">'.$aRow['descstatus'].'</span>';
		$btn = '<a href="#" onClick="showModals(\''.$aRow['idkey'].'\')">Edit</a> | <a href="#" onClick="deleteData(\''.$aRow['idkey'].'\')">Hapus</a>';
		for ( $i=0 ; $i<$iColumnCount ; $i++ ) {
			$row[] = $aRow[ $aColumns[$i] ];
		}
		$row = array( $btn, $aRow['kode'], $aRow['namakota'], $status);
		$output['aaData'][] = $row;
	}
	echo json_encode( $output );

?>

