<?php
	mb_internal_encoding('UTF-8');	
	$lib='../../../lib/';
	$aColumns = array( 'idkey','notrans','tgltrans','pemasokid','namapemasok','jumlah','jatuhtempo','matauang','keterangan','haritempo','colorstatus','descstatus'); 
	$sIndexColumn = 'idkey';
	$sTable = '(SELECT
				hutang.idtrans AS idkey,
				hutang.kodetrans,
				hutang.notrans,
				DATE_FORMAT(hutang.tgltrans,"%d/%m/%Y") AS tgltrans,
				DATE_FORMAT(hutang.jatuhtempo,"%d/%m/%Y") AS jatuhtempo,
				hutang.matauang,
				hutang.pemasokid,
				pemasok.namapemasok,
				FORMAT(hutang.jumlah,0) AS jumlah,
				hutang.keterangan,
				hutang.status,
				DATEDIFF(hutang.jatuhtempo, NOW()) AS haritempo,
				CASE WHEN hutang.status = 0 THEN "green" ELSE "red" END AS colorstatus,
				CASE WHEN hutang.status = 0 THEN "Validasi" ELSE "Batal" END AS descstatus
				FROM
				hutang
				LEFT JOIN pemasok ON hutang.pemasokid = pemasok.idpemasok) DERIVEDTBL'; 
	include_once ''.$lib.'Database.php';
	include_once ''.$lib.'showCore.php';
	while ( $aRow = $rResult->fetch_assoc() ) {
		$row = array(); 

		if($aRow['haritempo'] < 4 ) {
			$action= '<a class="btn btn-warning btn-xs" href="index.php?mod=bayarhutang_odetail&key='.$aRow['pemasokid'].'"> Bayar</a>';
		}else{
			$action = '';
		};

		$status= '<span class="badge bg-'.$aRow['colorstatus'].'">'.$aRow['descstatus'].'</span>';
		$btn = '<a href="#" onClick="showModals(\''.$aRow['idkey'].'\')">Edit</a> | <a href="#" onClick="deleteData(\''.$aRow['idkey'].'\')">Delete</a>';
		for ( $i=0 ; $i<$iColumnCount ; $i++ ) {
			$row[] = $aRow[ $aColumns[$i] ];
		}
		$row = array( $btn, $aRow['notrans'], $aRow['tgltrans'], $aRow['namapemasok'], $aRow['jatuhtempo'], $aRow['haritempo'], $aRow['matauang'], $aRow['jumlah'], $aRow['keterangan'],$status,$action);
		$output['aaData'][] = $row;
	}
	echo json_encode( $output );

?>

