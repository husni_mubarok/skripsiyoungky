<?php
$db=new Database();
$subnavmenu = new Subnavmenu();
$moddetail=$_GET['mod'];
$modulename=str_replace("_odetail","", $moddetail);
$datasubnavmenu = $subnavmenu->showsubnavmenu($modulename);
extract($datasubnavmenu);

${"$modulename"} = new Bayarhutang();
$key = $_GET['key'];
//Show header
$dataheader = ${"$modulename"}->showOpenHeader($key);
extract($dataheader);
//Show detail
$datadetail = ${"$modulename"}->showOpenDetail($key);
extract($datadetail);
if(isset($_REQUEST['submit'])){
	extract($_REQUEST);
	${"$modulename"}->pickheader($kodetrans, $notrans, $tgltrans, $bankid, $norekening);
	${"$modulename"}->pickdetail($idtrans, $totalpick); 
} 

?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header" style="margin-top:-15px !important; margin-bottom:-20px !important">
    <h4>
      <?php echo $subnavmenuname; ?> Open
    </h4>
    <ol class="breadcrumb">
      <li><a href="index.php"><i class="fa fa-dashboard"></i> Home</a></li>
      <li><a href="#"><?php echo $navmenuname; ?></a></li>
      <li class="active"><?php echo $subnavmenuname; ?></li>
    </ol>
  </section>
  <!-- Main content -->
  <section class="content">
   <form id="form" name="formproduk"  class="editprofileform" method="post" action="" enctype="multipart/form-data" onsubmit="return validateForm()">
    <div class="row">
      <div class="col-xs-12">
       <div class="box box-danger">
        <div class="box-header with-border">
         <div class="row">
          <!-- Coloumn 1-->
          <div class="col-md-4">
            <div class="form-group">
              <label for="real_name" class="col-sm-3 control-label">Pemasok</label>
              <div class="col-sm-9">
                <input type="text" class="form-control" value="<?php echo $namapemasok; ?>">
              </div>
            </div>
          </div>
          <div class="col-md-4">
            <div class="form-group">
              <label for="real_name" class="col-sm-3 control-label">Bank</label>
              <div class="col-sm-9">
                <input type="text" class="form-control" value="<?php echo $namabank; ?>">
                <input type="hidden" name="bankid" class="form-control" value="<?php echo $bankid; ?>">
              </div>
            </div>
          </div>
          <div class="col-md-4">
            <div class="form-group">
              <label for="real_name" class="col-sm-3 control-label">No Rekening</label>
              <div class="col-sm-9">
                <input type="text" class="form-control" name="norekening" value="<?php echo $norekening; ?>">
              </div>
            </div>
          </div>
          <!-- Coloumn 2-->               
          <div class="col-md-4" style="margin-bottom:-20px !important">
          </div>
          <!-- Coloumn 3-->                                   
          <div class="col-md-4">
          </div>
        </div>
      </div><!-- /.box-header -->

      <div class="box-body">
        <table id="example1" class="table table-bordered table-striped">
          <thead>
            <tr>
              <th>No</th>
              <th>No Hutang</th>
              <th>Tgl Hutang</th>
              <th>Mata Uang</th>
              <th>Jatuh Tempo</th>
              <th>Jumlah Hutang</th>
              <th>Jumlah Sisa</th>
              <th><input type="checkbox" name="selectall" id="selectall" value="" onchange="calculate()"/></th>
              <th>Total Pick</th>
            </tr>
          </thead>
          <tbody>
            <?php
            $i=1;
            if($datadetail !=0){
              foreach ($datadetail as $valuepurorder) {
                extract($valuepurorder);
                ?>    
                <tr>
                  <td><?php echo $i; ?></td>
                  <td class="left"><?php echo $notrans; ?></td>
                  <td class="left"><?php echo $tgltrans; ?></td>
                  <td class="left"><?php echo $matauang; ?></td>
                  <td class="left"><?php echo $jatuhtempo; ?></td>
                  <td class="left"><?php echo number_format($jumlah); ?></td> 
                  <td class="left"><?php echo number_format($sisa); ?></td>
                  <td class="centeralign">
                    <input type="checkbox" name="checkedid[]" class="checkbox" id="check" value="<?PHP echo $idtrans;?>" onchange="myFunction(this)">
                  </td>
                  <td>
                    <input type="text" class="form-control input-sm" style="height:22px; margin-bottom:-3px; margin-top:-4px !important" name="totalpick[]" id="totalpick<?PHP echo $idtrans;?>" value="<?PHP echo $sisa;?>" disabled="disabled"  onkeyup="numericFilter(this); updateSum();  valpick<?PHP echo $idtrans;?>();" >
                  </td>
                </tr>

                <input type="text" style="width:1px !important; height:1px !important; border-color:#FFFFFF !important; border-style:none" id="totalpickc<?PHP echo $idtrans;?>" value="<?PHP echo $sisa;?>">
                <script type="text/javascript">
                  function valpick<?PHP echo $idtrans;?>(){
                    //console.log('sajdjkad');
                    var q1<?PHP echo $idtrans;?> = +document.getElementById("totalpickc<?PHP echo $idtrans;?>").value;
                    var q2<?PHP echo $idtrans;?> = +document.getElementById("totalpick<?PHP echo $idtrans;?>").value;
                    if (q1<?PHP echo $idtrans;?> < q2<?PHP echo $idtrans;?>){
                      alert ("Pembayaran hutang melebihi nominal hutang!");

                      document.getElementById("totalpick<?PHP echo $idtrans;?>").value = document.getElementById("totalpickc<?PHP echo $idtrans;?>").value;
                      updateSum();
                    }
                  };
                </script>
                <?php
                $i++;
              }
            } 
            ?>   
          </tbody> 
          <tfoot>
            <tr>
              <th></th>
              <th></th>
              <th></th>
              <th></th>
              <th></th>
              <th></th>
              <th></th>
              <th>Total</th>
              <th ><div id="sum"></div></th>
            </tr>
          </tfoot>
        </table>
        <a href="#" data-toggle="modal" data-easein="swoopIn" data-target=".MyModals" type="button"  class="btn btn-primary  btn-flat pull-right" onclick="showModalsPick()"><i class="fa fa-check"></i> Pick</a>
      </div><!-- /.box-body -->
    </div>
  </div>
</div><!-- /.col -->
</div><!-- /.row --> 
</section><!-- /.content -->
</div>

<!-- Modal Popup -->
<div class="modal fade MyModals" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" >
  <div class="modal-dialog" style="width:420px !important;">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Pilih Kode Transaksi</h4>
      </div>
      <div class="modal-body">
        <input type="hidden" class="form-control" id="idperiod" name="idperiod">
        <input type="hidden" class="form-control" id="type" name="type">
        <div class="form-group">
          <label for="real_name" class="col-sm-3 control-label">Transaksi</label>
          <div class="col-sm-8">
            <select class="form-control"  style="width: 100%;" name="kodetrans" id="kodetrans"  placeholder="Transaction Code">
              <option value="APPY">Bayar Hutang</option>
            </select>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button class="btn btn-warning btn-flat">Submit</button>
        <input type="hidden" value="1" name="submit" />
        <button type="button" onclick="ClearVal()" class="btn btn-default btn-flat pull-left" data-dismiss="modal">Close</button>
      </div>
    </form>
  </div>
</div>
</div>

<?php
echo "<link rel='stylesheet' type='text/css' href='modules/".str_replace("_odetail","", $modulename )."/css/open/page.css' />";
echo "<script type='text/javascript' src='modules/".str_replace("_odetail","", $modulename )."/js/open/page.js'></script>";
?>
