<?php
	mb_internal_encoding('UTF-8');	
	$lib='../../../../lib/';
	$aColumns = array( 'idkey','namapemasok','alamat1','namakota','kodepos','tlp1','fax','email','sisa'); 
	$sIndexColumn = 'idkey';
	$sTable = '(SELECT
					hutang.pemasokid AS idkey,
					pemasok.namapemasok,
					pemasok.tlp1,
					pemasok.email,
					pemasok.fax,
					pemasok.alamat1,
					kota.namakota,
					pemasok.kodepos,
					FORMAT(SUM(
						hutang.jumlah - IFNULL(bayarhutangdet.jumlah, 0)
					),0) AS sisa
				FROM
					hutang
				INNER JOIN pemasok ON hutang.pemasokid = pemasok.idpemasok
				LEFT JOIN kota ON pemasok.kotaid = kota.idkota
				LEFT JOIN bayarhutangdet ON hutang.idtrans = bayarhutangdet.hutangid
				GROUP BY
					hutang.pemasokid,
					pemasok.namapemasok,
					pemasok.tlp1,
					pemasok.email,
					pemasok.fax,
					pemasok.alamat1,
					kota.namakota,
					pemasok.kodepos
				HAVING
					SUM(
						hutang.jumlah - IFNULL(bayarhutangdet.jumlah, 0)
					) > 0) DERIVEDTBL'; 
	include_once ''.$lib.'Database.php';
	include_once ''.$lib.'showCore.php';
	while ( $aRow = $rResult->fetch_assoc() ) {
		$row = array();
		$btn = '<a href="index.php?mod=bayarhutang_odetail&key='.$aRow['idkey'].'">'.$aRow['namapemasok'].'</a>';
		for ( $i=0 ; $i<$iColumnCount ; $i++ ) {
			$row[] = $aRow[ $aColumns[$i] ];
		}
		$row = array($btn, $aRow['alamat1'], $aRow['namakota'], $aRow['kodepos'], $aRow['tlp1'], $aRow['fax'], $aRow['email'], $aRow['sisa']);
		$output['aaData'][] = $row;
	}
	echo json_encode( $output );

?>

