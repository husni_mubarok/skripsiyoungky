<?php
	mb_internal_encoding('UTF-8');	
	$lib='../../../../lib/';
	$table="bayarhutang";
	$descstatus = "CASE WHEN ".$table.".status = 0 THEN 'Open' ELSE CASE WHEN ".$table.".status = 1 THEN 'Validate' ELSE CASE WHEN ".$table.".status = 9 THEN 'Cancel' END END END";
	$colorstatus = "CASE WHEN ".$table.".status = 0 THEN 'orange' ELSE CASE WHEN ".$table.".status = 1 THEN 'green' ELSE CASE WHEN ".$table.".status = 9 THEN 'red' END END END";
	$aColumns = array( 'idtrans','notrans','tgltrans','namapemasok','matauang','keterangan','norekening','namabank','colorstatus','descstatus'); 
	$sIndexColumn = 'idtrans';
	$sTable = '(SELECT
				bayarhutang.idtrans,
				bayarhutang.kodetrans,
				bayarhutang.notrans,
				bayarhutang.tgltrans,
				bayarhutang.pemasokid,
				pemasok.namapemasok,
				bayarhutang.matauang,
				bayarhutang.keterangan AS keterangan,
				bayarhutang.jenisbayarid,
				jenisbayar.namajenisbayar,
				bayarhutang.norekening,
				bayarhutang.jumlah,
				bayarhutang.bankid,
				bank.namabank,
				'.$colorstatus.' AS colorstatus,
				'.$descstatus.' AS descstatus
				FROM
				bayarhutang
				LEFT JOIN pemasok ON bayarhutang.pemasokid = pemasok.idpemasok
				LEFT JOIN jenisbayar ON jenisbayar.idjenisbayar = bayarhutang.jenisbayarid
				LEFT JOIN bank ON bayarhutang.bankid = bank.idbank) DERIVEDTBL'; 
	include_once ''.$lib.'Database.php';
	include_once ''.$lib.'showCore.php';
	while ( $aRow = $rResult->fetch_assoc() ) {
		$row = array();
		$status= '<span class="badge bg-'.$aRow['colorstatus'].'">'.$aRow['descstatus'].'</span>';
		$btn = '<a href="index.php?mod=bayarhutang_ldetail&key='.$aRow['idtrans'].'">'.$aRow['notrans'].'</a>';
		for ( $i=0 ; $i<$iColumnCount ; $i++ ) {
			$row[] = $aRow[ $aColumns[$i] ];
		}
		$row = array($btn, $aRow['tgltrans'], $aRow['namapemasok'], $aRow['matauang'], $aRow['namabank'], $aRow['norekening'], $aRow['keterangan']);
		$output['aaData'][] = $row;
	}
	echo json_encode( $output );

?>

