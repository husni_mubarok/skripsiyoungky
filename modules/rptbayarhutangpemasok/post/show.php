<?php
	$user = $_GET['user'];
	mb_internal_encoding('UTF-8');	
	$lib='../../../lib/';
	$aColumns = array( 'bulan','namapemasok','jumlah'); 
	$sIndexColumn = 'bulan';
	$sTable = '(SELECT
					MONTHNAME(bayarhutang.tgltrans) AS bulan,
					pemasok.namapemasok,
					SUM(bayarhutangdet.jumlah) AS jumlah
				FROM
					bayarhutang
				LEFT JOIN pemasok ON bayarhutang.pemasokid = pemasok.idpemasok
				INNER JOIN bayarhutangdet ON bayarhutang.idtrans = bayarhutangdet.transid,
				 users
				WHERE
					users.username = "'.$user.'"
				AND bayarhutang.tgltrans BETWEEN users.begindate
				AND users.enddate
				GROUP BY
					MONTHNAME(bayarhutang.tgltrans),
					pemasok.namapemasok) DERIVEDTBL'; 
	include_once ''.$lib.'Database.php';
	include_once ''.$lib.'showCore.php';
	while ( $aRow = $rResult->fetch_assoc() ) {
		$row = array();
		for ( $i=0 ; $i<$iColumnCount ; $i++ ) {
			$row[] = $aRow[ $aColumns[$i] ];
		}

		$row = array( $aRow['bulan'], $aRow['namapemasok'], $aRow['jumlah']);
		$output['aaData'][] = $row;
	}
	echo json_encode( $output );

?>
