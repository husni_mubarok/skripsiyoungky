<?php
	$user = $_GET['user'];
	mb_internal_encoding('UTF-8');	
	$lib='../../../lib/';
	$aColumns = array( 'namapemasok','jumlah30','jumlah60','jumlah90'); 
	$sIndexColumn = 'namapemasok';
	$sTable = '(SELECT
					pemasok.namapemasok,
					CASE
				WHEN DATEDIFF(NOW(), hutang.jatuhtempo) BETWEEN 0
				AND 30 THEN
					SUM(hutang.jumlah)
				ELSE
					0
				END AS jumlah30,
				 CASE
				WHEN DATEDIFF(NOW(), hutang.jatuhtempo) BETWEEN 31
				AND 60 THEN
					SUM(hutang.jumlah)
				ELSE
					0
				END AS jumlah60,
				 CASE
				WHEN DATEDIFF(NOW(), hutang.jatuhtempo) BETWEEN 61
				AND 90 THEN
					SUM(hutang.jumlah)
				ELSE
					0
				END AS jumlah90
				FROM
					hutang
				LEFT JOIN pemasok ON hutang.pemasokid = pemasok.idpemasok
				LEFT JOIN (
					SELECT
						hutangid,
						SUM(jumlah) AS jumlahbayar
					FROM
						bayarhutangdet
					GROUP BY
						hutangid
				) AS bayarhutang ON hutang.idtrans = bayarhutang.hutangid 
				WHERE  hutang.jumlah - IFNULL(bayarhutang.jumlahbayar, 0) > 0) DERIVEDTBL'; 
	include_once ''.$lib.'Database.php';
	include_once ''.$lib.'showCore.php';
	while ( $aRow = $rResult->fetch_assoc() ) {
		$row = array();
		for ( $i=0 ; $i<$iColumnCount ; $i++ ) {
			$row[] = $aRow[ $aColumns[$i] ];
		}

		$row = array( $aRow['namapemasok'], $aRow['jumlah30'], $aRow['jumlah60'], $aRow['jumlah90']);
		$output['aaData'][] = $row;
	}
	echo json_encode( $output );

?>
