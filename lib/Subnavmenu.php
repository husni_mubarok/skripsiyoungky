<?php
include_once 'Database.php';
class subnavmenu {
    private $db = '';
    private $data;
    public function __construct() {
        $this->db = new Database();
    }
    function showsubnavmenu($modulename) {
		$user=$_SESSION['uname'];
        $sql = "SELECT
				subnavmenu.idsubnavmenu,
				subnavmenu.subnavmenuname,
				navmenu.navmenuname,
				subnavmenu.submodule
				FROM
				subnavmenu
				INNER JOIN navmenu ON subnavmenu.navmenuid = navmenu.idnavmenu WHERE subnavmenu.submodule=:modulename";
        $arrData = array(':modulename' => $modulename);
        $this->data = $this->db->getById($sql, $arrData);
        return $this->data;
    }
	
	
		function showfilter() {
		$user=$_SESSION['uname'];
        $sql = "SELECT
				DATE_FORMAT(users.begindate,'%d/%m/%Y') AS fbegindate,
				DATE_FORMAT(users.enddate,'%d/%m/%Y') AS fenddate
				FROM
				users
				WHERE 
				users.username='".$user."'";
		$arrData = array();
        $this->data = $this->db->getById($sql, $arrData);
        return $this->data;
    }

}
?>
