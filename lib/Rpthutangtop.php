	<?php
	include_once 'Database.php';
	class rpthutangtop {
    private $db ='';
    private $data;
    public function __construct(){
        $this->db = new Database();
    }

	function show($rptdatefrom, $rptdateto){
		$rptdatefrom = $_POST['rptdatefrom']; 
		$date = str_replace('/', '-', $rptdatefrom);
		$rptdatefrom = date('Y-m-d', strtotime($date));
		
		$rptdateto = $_POST['rptdateto']; 
		$date = str_replace('/', '-', $rptdateto);
		$rptdateto = date('Y-m-d', strtotime($date));

        $sql = "SELECT
					pemasok.namapemasok,
					CASE
				WHEN DATEDIFF(NOW(), hutang.jatuhtempo) BETWEEN 0
				AND 30 THEN
					SUM(hutang.jumlah)
				ELSE
					0
				END AS jumlah30,
				 CASE
				WHEN DATEDIFF(NOW(), hutang.jatuhtempo) BETWEEN 31
				AND 60 THEN
					SUM(hutang.jumlah)
				ELSE
					0
				END AS jumlah60,
				 CASE
				WHEN DATEDIFF(NOW(), hutang.jatuhtempo) BETWEEN 61
				AND 90 THEN
					SUM(hutang.jumlah)
				ELSE
					0
				END AS jumlah90
				FROM
					hutang
				LEFT JOIN pemasok ON hutang.pemasokid = pemasok.idpemasok
				LEFT JOIN (
					SELECT
						hutangid,
						SUM(jumlah) AS jumlahbayar
					FROM
						bayarhutangdet
					GROUP BY
						hutangid
				) AS bayarhutang ON hutang.idtrans = bayarhutang.hutangid 
				WHERE hutang.jumlah - IFNULL(bayarhutang.jumlahbayar, 0) > 0";
        $arrData = array(':rptdatefrom' => $rptdatefrom, ':rptdateto' => $rptdateto);
        $this->data = $this->db->searchData($sql, $arrData);
        return $this->data;
    }

    function showtotal($rptdatefrom, $rptdateto){
    	$rptdatefrom = $_POST['rptdatefrom']; 
		$date = str_replace('/', '-', $rptdatefrom);
		$rptdatefrom = date('Y-m-d', strtotime($date));
		
		$rptdateto = $_POST['rptdateto']; 
		$date = str_replace('/', '-', $rptdateto);
		$rptdateto = date('Y-m-d', strtotime($date));
		
        $sql = "SELECT
					SUM(hutang.jumlah) AS tsumjumlah,
					SUM(bayarhutang.jumlahbayar) AS tsumjumlahbayar,
					SUM(
						hutang.jumlah - IFNULL(bayarhutang.jumlahbayar, 0)
					) AS tsumsisa
				FROM
					hutang
				LEFT JOIN pemasok ON hutang.pemasokid = pemasok.idpemasok
				LEFT JOIN (
					SELECT
						hutangid,
						SUM(jumlah) AS jumlahbayar
					FROM
						bayarhutangdet
					GROUP BY
						hutangid
				) AS bayarhutang ON hutang.idtrans = bayarhutang.hutangid
				WHERE
					hutang.tgltrans BETWEEN :rptdatefrom
				AND :rptdateto
				AND hutang.jumlah - IFNULL(bayarhutang.jumlahbayar, 0) > 0";
        $arrData = array(':rptdatefrom' => $rptdatefrom, ':rptdateto' => $rptdateto);
        $this->data = $this->db->searchData($sql, $arrData);
        return $this->data;
    }

	

}
?>

