<?php
include_once 'Database.php';
class bayarhutang {
    private $db = '';
    private $data;
	private $table = 'bayarhutang';
    public function __construct() {
        $this->db = new Database();
    }
	function showOpenHeader($key) {
        $sql = "SELECT
				pemasok.idpemasok,
				pemasok.namapemasok,
				pemasok.bankid,
				pemasok.norekening,
				bank.namabank
				FROM
				pemasok
				LEFT JOIN 
				bank ON pemasok.bankid = bank.idbank
				WHERE pemasok.idpemasok=:key";
        $arrData = array(':key' => $key);
        $this->data = $this->db->getById($sql, $arrData);
        return $this->data;
    }
	function showListHeader($key) {
        $sql = "SELECT
				bayarhutang.idtrans,
				bayarhutang.kodetrans,
				bayarhutang.notrans,
				bayarhutang.tgltrans,
				bayarhutang.pemasokid,
				pemasok.namapemasok,
				bayarhutang.matauang,
				bayarhutang.keterangan AS keterangan1,
				bayarhutang.jenisbayarid,
				jenisbayar.namajenisbayar,
				bayarhutang.norekening,
				bayarhutang.jumlah,
				bayarhutang.bankid,
				bank.namabank
				FROM
				bayarhutang
				LEFT JOIN pemasok ON bayarhutang.pemasokid = pemasok.idpemasok
				LEFT JOIN jenisbayar ON jenisbayar.idjenisbayar = bayarhutang.jenisbayarid
				LEFT JOIN bank ON bayarhutang.bankid = bank.idbank
				WHERE  bayarhutang.idtrans=:key";
        $arrData = array(':key' => $key);
        $this->data = $this->db->getById($sql, $arrData);
        return $this->data;
    }
	function showOpenDetail($key) {
        $sql = "SELECT
				hutang.idtrans,
				hutang.kodetrans,
				hutang.notrans,
				hutang.tgltrans,
				hutang.keterangan, 
				hutang.pemasokid,
                hutang.jumlah,
                hutang.matauang,
                hutang.jatuhtempo,
				hutang.jumlah - IFNULL(bayarhutang.jumlahbayar,0) AS sisa
				FROM
				hutang LEFT JOIN (SELECT hutangid, SUM(jumlah) AS jumlahbayar FROM bayarhutangdet GROUP BY hutangid) AS bayarhutang ON hutang.idtrans = bayarhutang.hutangid
				WHERE hutang.pemasokid=:key AND (hutang.jumlah - IFNULL(bayarhutang.jumlahbayar,0) >0)";
        $arrData = array(':key' => $key);
        $this->data = $this->db->searchData($sql, $arrData);
        return $this->data;
    }

    function pickheader($kodetrans, $notrans, $tgltrans, $bankid, $norekening) {
		$key = $_GET['key'];
        $sql = "INSERT INTO bayarhutang (kodetrans, notrans, tgltrans, pemasokid, bankid, norekening)
				SELECT :kodetrans,
				IFNULL(CONCAT(:kodetrans,'-',DATE_FORMAT(NOW(), '%d%m%y'),RIGHT((RIGHT(MAX(bayarhutang.notrans),3))+1001,3)), CONCAT(:kodetrans,'-',DATE_FORMAT(NOW(), '%d%m%y'),'001')), DATE(NOW()), '".$key."', :bankid, :norekening 
				FROM
				bayarhutang
				WHERE kodetrans=:kodetrans";
        $arrData = array(':kodetrans' => $kodetrans, ':notrans' => $notrans, ':tgltrans' => $tgltrans, ':bankid' => $bankid, ':norekening' => $norekening);
        $this->data = $this->db->insertData($sql, $arrData);
		if($this->data == true){
			$key = $this->db->lastID();
            header("location:index.php?mod=bayarhutang_ldetail&key=$key");
        }		
    }
	
	function pickdetail($idtrans, $totalpick) {
		$key = $this->db->lastID();		
		$count = count($_POST['checkedid']);
		for($i=0;$i<=$count;$i++) {
		$idtrans= $_POST['checkedid'][$i];
		$totalpick= $_POST['totalpick'][$i];
        $sql = "INSERT INTO bayarhutangdet (hutangid, transid, jumlah)
				SELECT
				:idtrans,
				'".$key."',
				:totalpick
				FROM
				hutang 
				WHERE hutang.idtrans = '".$idtrans."'";
        $arrData = array(':idtrans' => $idtrans, ':totalpick' => $totalpick);
        $this->data = $this->db->insertData($sql, $arrData);
		} 
		if ($this->data == true) {
            
        }
		return $this->data;
    }
    function saveclose($key, $tgltrans, $jenisbayarid, $pemasokid, $matauang, $keterangan, $bankid, $norekening) {
		$pemasokid=$_POST['pemasokid'];
		$moddetail=$_GET['mod'];
		$modulename=str_replace("_ldetail","", $moddetail );
		
		$vardatetime = $_POST['tgltrans']; 
		$date = str_replace('/', '-', $vardatetime);
		$tgltrans = date('Y-m-d', strtotime($date));
				
        $sql = "UPDATE bayarhutang SET tgltrans=:tgltrans, jenisbayarid=:jenisbayarid, pemasokid=:pemasokid, matauang=:matauang, keterangan=:keterangan, bankid=:bankid, norekening=:norekening WHERE idtrans=:key";
        $arrData = array(':tgltrans' => $tgltrans, ':jenisbayarid' => $jenisbayarid, ':pemasokid' => $pemasokid, 
		':matauang' => $matauang, ':keterangan' => $keterangan, ':bankid' => $bankid, ':norekening' => $norekening, ':key' => $key);
        $this->data = $this->db->insertData($sql, $arrData);
        if ($this->data == true) {
            header("location:index.php?mod=$modulename");
        }
        return $this->data;
    }
	function savevalidate($key, $tgltrans, $paytypeid, $pemasokid, $accountid, $currency, $remark) {
		$pemasokid=$_POST['pemasokid'];
		$moddetail=$_GET['mod'];
		$modulename=str_replace("_ldetail","", $moddetail );
		
		$vardatetime = $_POST['tgltrans']; 
		$date = str_replace('/', '-', $vardatetime);
		$tgltrans = date('Y-m-d', strtotime($date));
				
        $sql = "UPDATE arinvoice SET tgltrans=:tgltrans, remark=:remark, status=1 WHERE idtrans=:key";
        $arrData = array(':tgltrans' => $tgltrans, ':remark' => $remark, ':key' => $key);
        $this->data = $this->db->insertData($sql, $arrData);
        if ($this->data == true) {
            header("location:index.php?mod=$modulename");
        }
        return $this->data;
    }
	function shipment($key) {	
		$moddetail=$_GET['mod'];
		$modulename=str_replace("_detail","", $moddetail );	
				
        $sql = "UPDATE arinvoice SET status=2 WHERE idtrans=:key";
        $arrData = array(':key' => $key);
        $this->data = $this->db->insertData($sql, $arrData);
        if ($this->data == true) {
            header("location:index.php?mod=$modulename");
        }
        return $this->data;
    }

	function cancel($key) {
		$pemasokid=$_POST['pemasokid'];
		$moddetail=$_GET['mod'];
		$modulename=str_replace("_ldetail","", $moddetail );
        $sql = "UPDATE $this->table SET status=9 WHERE idtrans=:key";
        $arrData = array(':key' => $key);
        $this->data = $this->db->insertData($sql, $arrData);
        if ($this->data == true) {
            header("location:index.php?mod=$modulename");
        }
        return $this->data;
    }
	function delete($key) {
		$pemasokid=$_POST['pemasokid'];
		$moddetail=$_GET['mod'];
		$modulename=str_replace("_ldetail","", $moddetail );
        $sql = "DELETE FROM $this->table WHERE idtrans=:key";
        $arrData = array(':key' => $key);
        $this->data = $this->db->deleteData($sql, $arrData);
        if ($this->data == true) {
            header("location:index.php?mod=$modulename");
        }
    }
	function showslipdetail($key) {
        $sql = "SELECT
				arinvoicedet.idtransdet,
				arinvoicedet.transid,
				arinvoicedet.inventoryid,
				inventory.inventorycode,
				inventory.inventoryname,
				arinvoicedet.unit,
				arinvoicedet.unitprice,
				arinvoicedet.discount,
				arinvoicedet.amount,
				arinvoicedet.quantity,
				salesorder.notrans,
				salesorder.tgltrans
				FROM
				arinvoicedet
				INNER JOIN inventory ON arinvoicedet.inventoryid = inventory.idinventory
				INNER JOIN salesorderdet ON arinvoicedet.salesorderdetid = salesorderdet.idtransdet
				INNER JOIN salesorder ON salesorderdet.transid = salesorder.idtrans
				WHERE  arinvoicedet.transid=:key";
        $arrData = array(':key' => $key);
        $this->data = $this->db->searchData($sql, $arrData);
        return $this->data;
    }
	function showsliptotal($key) {
        $sql = "SELECT
				SUM(arinvoicedet.quantity) AS tquantity
				FROM
				arinvoicedet
				WHERE  arinvoicedet.transid=:key";
        $arrData = array(':key' => $key);
        $this->data = $this->db->searchData($sql, $arrData);
        return $this->data;
    }

}
?>
