<?php
include_once 'Database.php';
class combobox {
    private $db = '';
    private $data;
    public function __construct() {
        $this->db = new Database();
    }
    function showPeriod() {
        $sql = "SELECT * FROM period WHERE status=0";
        $this->data = $this->db->loadData($sql);
        return $this->data;
    }
    function showUsers() {
        $sql = "SELECT * FROM users";
        $this->data = $this->db->loadData($sql);
        return $this->data;
    }
	function showTicketStatus() {
        $sql = "SELECT * FROM ticketstatus WHERE status=0";
        $this->data = $this->db->loadData($sql);
        return $this->data;
    }
	function showAttribute() {
        $sql = "SELECT * FROM attribute WHERE status=0";
        $this->data = $this->db->loadData($sql);
        return $this->data;
    }
	function showPemasok() {
        $sql = "SELECT * FROM pemasok";
        $this->data = $this->db->loadData($sql);
        return $this->data;
    }
    function showTipepemasok() {
        $sql = "SELECT * FROM tipepemasok";
        $this->data = $this->db->loadData($sql);
        return $this->data;
    }
    function showKota() {
        $sql = "SELECT * FROM kota";
        $this->data = $this->db->loadData($sql);
        return $this->data;
    }
     function showMatauang() {
        $sql = "SELECT * FROM matauang";
        $this->data = $this->db->loadData($sql);
        return $this->data;
    }
    function showJenisbayar()
    {
        $sql = "SELECT * FROM jenisbayar";
        $this->data = $this->db->loadData($sql);
        return $this->data;
    }
    function showBank()
    {
        $sql = "SELECT * FROM bank";
        $this->data = $this->db->loadData($sql);
        return $this->data;
    }
}
?>
