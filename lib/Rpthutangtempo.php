	<?php
	include_once 'Database.php';
	class rpthutangtempo {
    private $db ='';
    private $data;
    public function __construct(){
        $this->db = new Database();
    }

	function show($rptdatefrom, $rptdateto){
		$rptdatefrom = $_POST['rptdatefrom']; 
		$date = str_replace('/', '-', $rptdatefrom);
		$rptdatefrom = date('Y-m-d', strtotime($date));
		
		$rptdateto = $_POST['rptdateto']; 
		$date = str_replace('/', '-', $rptdateto);
		$rptdateto = date('Y-m-d', strtotime($date));

        $sql = "SELECT
					hutang.idtrans AS idkey,
					hutang.kodetrans,
					hutang.notrans,
					hutang.pemasokid,
					pemasok.namapemasok,
					hutang.jatuhtempo,
					FORMAT(hutang.jumlah, 0) AS jumlah,
					hutang.keterangan,
					FORMAT(
						IFNULL(bayarhutang.jumlahbayar, 0),
						0
					) AS dibayar,
					FORMAT(
						hutang.jumlah - IFNULL(bayarhutang.jumlahbayar, 0),
						0
					) AS sisa
				FROM
					hutang
				LEFT JOIN pemasok ON hutang.pemasokid = pemasok.idpemasok
				LEFT JOIN (
					SELECT
						hutangid,
						SUM(jumlah) AS jumlahbayar
					FROM
						bayarhutangdet
					GROUP BY
						hutangid
				) AS bayarhutang ON hutang.idtrans = bayarhutang.hutangid
				WHERE
					hutang.tgltrans BETWEEN :rptdatefrom
				AND :rptdateto
				AND hutang.jumlah - IFNULL(bayarhutang.jumlahbayar, 0) > 0";
        $arrData = array(':rptdatefrom' => $rptdatefrom, ':rptdateto' => $rptdateto);
        $this->data = $this->db->searchData($sql, $arrData);
        return $this->data;
    }

    function showtotal($rptdatefrom, $rptdateto){
    	$rptdatefrom = $_POST['rptdatefrom']; 
		$date = str_replace('/', '-', $rptdatefrom);
		$rptdatefrom = date('Y-m-d', strtotime($date));
		
		$rptdateto = $_POST['rptdateto']; 
		$date = str_replace('/', '-', $rptdateto);
		$rptdateto = date('Y-m-d', strtotime($date));
		
        $sql = "SELECT 
					FORMAT(
						SUM(hutang.jumlah - IFNULL(bayarhutang.jumlahbayar, 0)),
						0
					) AS t_sisa
				FROM
					hutang
				LEFT JOIN pemasok ON hutang.pemasokid = pemasok.idpemasok
				LEFT JOIN (
					SELECT
						hutangid,
						SUM(jumlah) AS jumlahbayar
					FROM
						bayarhutangdet
					GROUP BY
						hutangid
				) AS bayarhutang ON hutang.idtrans = bayarhutang.hutangid
				WHERE
					hutang.tgltrans BETWEEN :rptdatefrom
				AND :rptdateto
				AND hutang.jumlah - IFNULL(bayarhutang.jumlahbayar, 0) > 0";
        $arrData = array(':rptdatefrom' => $rptdatefrom, ':rptdateto' => $rptdateto);
        $this->data = $this->db->searchData($sql, $arrData);
        return $this->data;
    }

	

}
?>

