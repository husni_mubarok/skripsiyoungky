	<?php
	include_once 'Database.php';
	class rptbayarhutangtahun {
    private $db ='';
    private $data;
    public function __construct(){
        $this->db = new Database();
    }

	function show($tahun){ 
		
		$tahun = $_POST['tahun'];  

        $sql = "SELECT
					MONTHNAME(bayarhutang.tgltrans) AS tahun,
					SUM(bayarhutangdet.jumlah) AS jumlah
				FROM
					bayarhutang
				LEFT JOIN pemasok ON bayarhutang.pemasokid = pemasok.idpemasok
				INNER JOIN bayarhutangdet ON bayarhutang.idtrans = bayarhutangdet.transid 
				WHERE 
				YEAR (bayarhutang.tgltrans) = :tahun
				GROUP BY
					MONTHNAME(bayarhutang.tgltrans)";
        $arrData = array(':tahun' => $tahun);
        $this->data = $this->db->searchData($sql, $arrData);
        return $this->data;
    }

    function showtotal($tahun){
    	$tahun = $_POST['tahun'];  
		
        $sql = "SELECT 
				FORMAT(SUM(bayarhutangdet.jumlah),0) AS tjumlah
				FROM
				bayarhutang
				INNER JOIN bayarhutangdet ON bayarhutang.idtrans = bayarhutangdet.transid
				WHERE YEAR (bayarhutang.tgltrans) = :tahun";
        $arrData = array(':tahun' => $tahun);
        $this->data = $this->db->searchData($sql, $arrData);
        return $this->data;
    }

	

}
?>

