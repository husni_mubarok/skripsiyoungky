	<?php
	include_once 'Database.php';
	class rpthutangoutstanding {
    private $db ='';
    private $data;
    public function __construct(){
        $this->db = new Database();
    }

	function show($rptdatefrom, $rptdateto){
		$rptdatefrom = $_POST['rptdatefrom']; 
		$date = str_replace('/', '-', $rptdatefrom);
		$rptdatefrom = date('Y-m-d', strtotime($date));
		
		$rptdateto = $_POST['rptdateto']; 
		$date = str_replace('/', '-', $rptdateto);
		$rptdateto = date('Y-m-d', strtotime($date));

        $sql = "SELECT
					pemasok.namapemasok,
					SUM(hutang.jumlah) AS sumjumlah,
					SUM(bayarhutang.jumlahbayar) AS sumjumlahbayar,
					SUM(hutang.jumlah - IFNULL(bayarhutang.jumlahbayar, 0)) AS sumsisa,
					GROUP_CONCAT(REPLACE(hutang.notrans,',','')) AS notrans,
					GROUP_CONCAT(REPLACE(hutang.jumlah,',','')) AS jumlah,
					GROUP_CONCAT(REPLACE(IFNULL(bayarhutang.jumlahbayar, 0),',','')) AS dibayar,
					GROUP_CONCAT(REPLACE(hutang.jumlah - IFNULL(bayarhutang.jumlahbayar, 0),',','')) AS sisa
				FROM
					hutang
				LEFT JOIN pemasok ON hutang.pemasokid = pemasok.idpemasok
				LEFT JOIN (
					SELECT
						hutangid,
						SUM(jumlah) AS jumlahbayar
					FROM
						bayarhutangdet
					GROUP BY
						hutangid
				) AS bayarhutang ON hutang.idtrans = bayarhutang.hutangid 
				WHERE hutang.tgltrans BETWEEN :rptdatefrom AND :rptdateto
				AND hutang.jumlah - IFNULL(bayarhutang.jumlahbayar, 0) > 0
				GROUP BY pemasok.namapemasok";
        $arrData = array(':rptdatefrom' => $rptdatefrom, ':rptdateto' => $rptdateto);
        $this->data = $this->db->searchData($sql, $arrData);
        return $this->data;
    }

    function showtotal($rptdatefrom, $rptdateto){
    	$rptdatefrom = $_POST['rptdatefrom']; 
		$date = str_replace('/', '-', $rptdatefrom);
		$rptdatefrom = date('Y-m-d', strtotime($date));
		
		$rptdateto = $_POST['rptdateto']; 
		$date = str_replace('/', '-', $rptdateto);
		$rptdateto = date('Y-m-d', strtotime($date));
		
        $sql = "SELECT
					SUM(hutang.jumlah) AS tsumjumlah,
					SUM(bayarhutang.jumlahbayar) AS tsumjumlahbayar,
					SUM(
						hutang.jumlah - IFNULL(bayarhutang.jumlahbayar, 0)
					) AS tsumsisa
				FROM
					hutang
				LEFT JOIN pemasok ON hutang.pemasokid = pemasok.idpemasok
				LEFT JOIN (
					SELECT
						hutangid,
						SUM(jumlah) AS jumlahbayar
					FROM
						bayarhutangdet
					GROUP BY
						hutangid
				) AS bayarhutang ON hutang.idtrans = bayarhutang.hutangid
				WHERE
					hutang.tgltrans BETWEEN :rptdatefrom
				AND :rptdateto
				AND hutang.jumlah - IFNULL(bayarhutang.jumlahbayar, 0) > 0";
        $arrData = array(':rptdatefrom' => $rptdatefrom, ':rptdateto' => $rptdateto);
        $this->data = $this->db->searchData($sql, $arrData);
        return $this->data;
    }

	

}
?>

