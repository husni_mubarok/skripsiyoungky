	<?php
	include_once 'Database.php';
	class rpthutanglunas {
    private $db ='';
    private $data;
    public function __construct(){
        $this->db = new Database();
    }

	function show($rptdatefrom, $rptdateto){
		$rptdatefrom = $_POST['rptdatefrom']; 
		$date = str_replace('/', '-', $rptdatefrom);
		$rptdatefrom = date('Y-m-d', strtotime($date));
		
		$rptdateto = $_POST['rptdateto']; 
		$date = str_replace('/', '-', $rptdateto);
		$rptdateto = date('Y-m-d', strtotime($date));

        $sql = "SELECT
					hutang.idtrans AS idkey,
					hutang.kodetrans,
					hutang.notrans,
					DATE_FORMAT(hutang.tgltrans, '%d/%m/%Y') AS tgltrans,
					DATE_FORMAT(
						hutang.jatuhtempo,
						'%d/%m/%Y'
					) AS jatuhtempo,
					hutang.matauang,
					hutang.pemasokid,
					pemasok.namapemasok,
					FORMAT(hutang.jumlah, 0) AS jumlah,
					hutang.keterangan,
					hutang. STATUS,
					FORMAT(
						IFNULL(bayarhutang.jumlahbayar, 0),
						0
					) AS dibayar,
					FORMAT(
						hutang.jumlah - IFNULL(bayarhutang.jumlahbayar, 0),
						0
					) AS sisa,
					CASE
				WHEN hutang.status = 0 THEN
					'green'
				ELSE
					'red'
				END AS colorstatus,
				 CASE
				WHEN hutang. STATUS = 0 THEN
					'Validasi'
				ELSE
					'Batal'
				END AS descstatus
				FROM
					hutang
				LEFT JOIN pemasok ON hutang.pemasokid = pemasok.idpemasok
				LEFT JOIN (
					SELECT
						hutangid,
						SUM(jumlah) AS jumlahbayar
					FROM
						bayarhutangdet
					GROUP BY
						hutangid
				) AS bayarhutang ON hutang.idtrans = bayarhutang.hutangid
								WHERE hutang.tgltrans BETWEEN :rptdatefrom AND :rptdateto
				AND
					hutang.jumlah - IFNULL(bayarhutang.jumlahbayar, 0) = 0";
        $arrData = array(':rptdatefrom' => $rptdatefrom, ':rptdateto' => $rptdateto);
        $this->data = $this->db->searchData($sql, $arrData);
        return $this->data;
    }

    function showtotal($rptdatefrom, $rptdateto){
    	$rptdatefrom = $_POST['rptdatefrom']; 
		$date = str_replace('/', '-', $rptdatefrom);
		$rptdatefrom = date('Y-m-d', strtotime($date));
		
		$rptdateto = $_POST['rptdateto']; 
		$date = str_replace('/', '-', $rptdateto);
		$rptdateto = date('Y-m-d', strtotime($date));
		
        $sql = "SELECT
					SUM(
						FORMAT(
							IFNULL(bayarhutang.jumlahbayar, 0),
							0
						)
					) AS t_dibayar
				FROM
					hutang
				LEFT JOIN pemasok ON hutang.pemasokid = pemasok.idpemasok
				LEFT JOIN (
					SELECT
						hutangid,
						SUM(jumlah) AS jumlahbayar
					FROM
						bayarhutangdet
					GROUP BY
						hutangid
				) AS bayarhutang ON hutang.idtrans = bayarhutang.hutangid
				WHERE hutang.tgltrans BETWEEN :rptdatefrom AND :rptdateto AND
					hutang.jumlah - IFNULL(bayarhutang.jumlahbayar, 0) = 0";
        $arrData = array(':rptdatefrom' => $rptdatefrom, ':rptdateto' => $rptdateto);
        $this->data = $this->db->searchData($sql, $arrData);
        return $this->data;
    }

	

}
?>

