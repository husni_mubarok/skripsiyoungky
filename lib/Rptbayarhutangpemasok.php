	<?php
	include_once 'Database.php';
	class rptbayarhutangpemasok {
    private $db ='';
    private $data;
    public function __construct(){
        $this->db = new Database();
    }

	function show($rptdatefrom, $rptdateto){
		$rptdatefrom = $_POST['rptdatefrom']; 
		$date = str_replace('/', '-', $rptdatefrom);
		$rptdatefrom = date('Y-m-d', strtotime($date));
		
		$rptdateto = $_POST['rptdateto']; 
		$date = str_replace('/', '-', $rptdateto);
		$rptdateto = date('Y-m-d', strtotime($date));

        $sql = "SELECT
					MONTHNAME(bayarhutang.tgltrans) AS bulan,
					pemasok.namapemasok,
					SUM(bayarhutangdet.jumlah) AS jumlah
				FROM
					bayarhutang
				LEFT JOIN pemasok ON bayarhutang.pemasokid = pemasok.idpemasok
				INNER JOIN bayarhutangdet ON bayarhutang.idtrans = bayarhutangdet.transid
				WHERE
					bayarhutang.tgltrans BETWEEN :rptdatefrom AND :rptdateto
				GROUP BY
					MONTHNAME(bayarhutang.tgltrans),
					pemasok.namapemasok";
        $arrData = array(':rptdatefrom' => $rptdatefrom, ':rptdateto' => $rptdateto);
        $this->data = $this->db->searchData($sql, $arrData);
        return $this->data;
    }

    function showtotal($rptdatefrom, $rptdateto){
    	$rptdatefrom = $_POST['rptdatefrom']; 
		$date = str_replace('/', '-', $rptdatefrom);
		$rptdatefrom = date('Y-m-d', strtotime($date));
		
		$rptdateto = $_POST['rptdateto']; 
		$date = str_replace('/', '-', $rptdateto);
		$rptdateto = date('Y-m-d', strtotime($date));
		
        $sql = "SELECT 
				FORMAT(SUM(bayarhutangdet.jumlah),0) AS tjumlah
				FROM
				bayarhutang
				INNER JOIN bayarhutangdet ON bayarhutang.idtrans = bayarhutangdet.transid
				WHERE bayarhutang.tgltrans BETWEEN :rptdatefrom AND :rptdateto";
        $arrData = array(':rptdatefrom' => $rptdatefrom, ':rptdateto' => $rptdateto);
        $this->data = $this->db->searchData($sql, $arrData);
        return $this->data;
    }

	

}
?>

