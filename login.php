<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>PT. CATUR MITRA SEJATI SENTOSA (MITRA10)</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.5 -->
    <link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">
    <!-- Font Awesome -->   
    <!-- Theme style -->
    <link rel="stylesheet" href="dist/css/AdminLTE.min.css">
    <!-- iCheck -->
    <link rel="stylesheet" href="plugins/iCheck/square/blue.css">

	</head>
  <body id="page" class="hold-transition login-page">
  	
  <header>
  <div class="container">
           
    <div class="login-box">
	
      <div class="login-logo">
	  <a href="#"><img src="assets/images/logo.png" alt="" /> </a> <br />

      </div><!-- /.login-logo -->
      <div class="login-box-body">
		<p> ANALISA DAN PERANCANGAN SISTEM TRANSAKSI PEMBAYARAN HUTANG ( OVERDUE ) BERBASIS WEB PADA PT. CATUR MITRA SEJATI SENTOSA (MITRA10) </p><br>
        <h4 class="login-box-msg"><b>Login Form</b></h4>
        <form action="loginact.php" method="post">
          <div class="form-group has-feedback">
            <input name="username" class="form-control" placeholder="Nama Pengguna">
            <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
          </div>
          <div class="form-group has-feedback">
            <input name="password"  type="password" class="form-control" placeholder="Kata Sandi">
            <span class="glyphicon glyphicon-lock form-control-feedback"></span>
          </div>
          <div class="row">  
            <div class="col-xs-4">
              <button type="submit" class="btn btn-primary btn-block btn-flat">Masuk</button>
            </div><!-- /.col -->
			<div class="col-xs-4">
			  <button type="reset" class="btn btn-primary btn-block btn-flat">Ulangi</button>
            </div><!-- /.col -->
          </div>
        </form>
		<br>
		<a class="btn btn-warning btn-flat btn-xs">1.0</a> <a href="#">Youngky</a><br>
      </div><!-- /.login-box-body -->
    </div><!-- /.login-box -->
 </div><!-- /.login-box-body -->
   
    <!-- jQuery 2.1.4 -->
    <script src="../../plugins/jQuery/jQuery-2.1.4.min.js"></script>
    <!-- Bootstrap 3.3.5 -->
    <script src="../../bootstrap/js/bootstrap.min.js"></script>

	</header>
  </body>
</html>

