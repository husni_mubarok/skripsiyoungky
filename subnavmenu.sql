-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jun 10, 2017 at 03:13 PM
-- Server version: 10.1.21-MariaDB
-- PHP Version: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `skripsiyoungky`
--

-- --------------------------------------------------------

--
-- Table structure for table `subnavmenu`
--

CREATE TABLE `subnavmenu` (
  `idsubnavmenu` int(11) NOT NULL,
  `subnavmenuname` varchar(50) NOT NULL,
  `navmenuid` int(50) NOT NULL,
  `sequencenumber` int(11) NOT NULL,
  `submodule` varchar(225) NOT NULL,
  `specialcharacter` varchar(2) NOT NULL DEFAULT '"'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `subnavmenu`
--

INSERT INTO `subnavmenu` (`idsubnavmenu`, `subnavmenuname`, `navmenuid`, `sequencenumber`, `submodule`, `specialcharacter`) VALUES
(1, 'Tambah Pengguna', 5, 3, 'adduser', '\"'),
(2, 'Ubah Password', 5, 2, 'changepassword', '\"'),
(3, 'Tipe Pengguna', 5, 3, 'usertype', '\"'),
(4, 'Otorisasi Pengguna', 5, 5, 'menuautorized', '\"'),
(135, 'Laporan Monitoring Hutang', 6, 1, 'monitoringhutang', '\"'),
(134, 'Bank', 1, 1, 'bank', '\"'),
(124, 'Tipe Pemasok', 1, 1, 'tipepemasok', '\"'),
(133, 'Pembayaran Hutang', 3, 2, 'bayarhutang', '\"'),
(132, 'Invoice Hutang', 3, 1, 'hutang', '\"'),
(123, 'Kota', 1, 1, 'kota', '\"'),
(130, 'Mata Uang', 1, 1, 'matauang', '\"'),
(131, 'Jenis Bayar', 1, 1, 'jenisbayar', '\"'),
(15, 'Pemasok', 1, 1, 'pemasok', '\"'),
(136, 'Laporan Pembayaran Hutang \n', 6, 2, 'rptbayarhutang', '\"'),
(137, 'Laporan Hutang Lunas', 6, 3, 'rpthutanglunas', '\"'),
(138, 'Laporan Hutang Outstanding', 6, 4, 'rpthutangoutstanding', '\"'),
(139, 'Laporan Pembayaran Hutang Pemasok', 6, 2, 'rptbayarhutangpemasok', '\"'),
(140, 'Laporan Hutang TOP', 6, 4, 'rpthutangtop', '\"'),
(141, 'Laporan Hutang Tempo', 6, 4, 'rpthutangtempo', '\"'),
(142, 'Laporan Pembayaran Hutang per Tahun\r\n', 6, 2, 'rptbayarhutangtahun', '\"');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `subnavmenu`
--
ALTER TABLE `subnavmenu`
  ADD PRIMARY KEY (`idsubnavmenu`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `subnavmenu`
--
ALTER TABLE `subnavmenu`
  MODIFY `idsubnavmenu` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=143;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
