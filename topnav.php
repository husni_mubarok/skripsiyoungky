<?php
$navmenu = new Navmenu();
$userprofile = new Users();
$core=new Core();
$data = $navmenu->show($user);
$dataconfigicon = $userprofile->showConfigIcon();
$datanotifcount = $navmenu->notifcount($user);
$datanotif = $navmenu->shownotif($user);
$datauserprofile = $userprofile->showProfile();
$modulename=$_GET['mod'];
$datanavmenu = $navmenu->shownavedit($modulename);
extract($datanavmenu);
$user=$_SESSION['uname'];
?>
<body class="hold-transition skin-blue sidebar-mini">
  <div class="wrapper">

    <header class="main-header">
      <!-- Logo -->
      <a href="index.php" class="logo">
        <!-- mini logo for sidebar mini 50x50 pixels -->
        <span class="logo-mini"><b>A</b>LT</span>
        <!-- logo for regular state and mobile devices -->
        <span class="logo-lg">PT. Catur Mitra Sejati Sentosa</span>
      </a>
      <!-- Header Navbar: style can be found in header.less -->
      <nav class="navbar navbar-static-top">
        <!-- Sidebar toggle button-->
        <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
          <span class="sr-only">Toggle navigation</span>
        </a>

        <div class="navbar-custom-menu">
         <ul class="nav navbar-nav">
          <!-- User Account: style can be found in dropdown.less -->
      <li class="dropdown user user-menu">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
         <?php
         $i=1;
         if($datauserprofile !=0){
           foreach ($datauserprofile as $valueuserprofile) {
             extract($valueuserprofile);
             ?>    
             <img src="assets/images/imageprofile/<?php echo $imageprofile; ?>" class="user-image" alt="User Image">
             <span class="hidden-xs"> <i class="fa fa-circle text-success"></i> <?php echo strtoupper($_SESSION['uname'])?> </span>
             
           </a>
           
           <ul class="dropdown-menu">
            <!-- User image -->
            <li class="user-header">
              <img src="assets/images/imageprofile/<?php echo $imageprofile; ?>" class="img-circle" alt="User Image">
              <p>
                <?php echo $name; ?>
                <small>Registered from <?php echo $recorded; ?></small>
              </p>
            </li>
            <!-- Menu Body -->
            <!-- Menu Footer-->
            <li class="user-footer">
              <div class="pull-left">
                <a href="?mod=adduser_update&key=<?php echo $_SESSION['uname']?>" class="btn btn-default btn-flat">Profile</a>
              </div>
              <div class="pull-right">
                <a href="logout.php" class="btn btn-default btn-flat">Log Out</a>
              </div>
            </li>
          </ul>
          <?php
          $i++;
        }
      }
      ?>   
    </li>
          </ul>
        </div>
      </nav>
    </header>
    <!-- Left side column. contains the logo and sidebar -->
    <aside class="main-sidebar">
      <!-- sidebar: style can be found in sidebar.less -->
      <section class="sidebar">
        <!-- Sidebar user panel -->
        <div class="user-panel">
          <div class="pull-left image">
            <img src="dist/img/user2-160x160.jpg" class="img-circle" alt="User Image">
          </div>
          <div class="pull-left info">
            <p>ADMIN</p>
            <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
          </div>
          </div>
        <!-- /.search form -->
        <!-- sidebar menu: : style can be found in sidebar.less -->
        <ul class="sidebar-menu">
          <li class="header">MAIN NAVIGATION</li>
          <?php
          $i=1;
          if($data !=0){
            foreach ($data as $value) {
              extract($value);
              $subnavmenuname = split(",",$value["subnavmenuname"]);
              ?>  
              <li class="treeview">

                <a href="#">
                  <i class="fa <?php echo $iclass; ?>"> <?php echo $navmenuname; ?></i>
                  <span class="pull-right-container"><?php echo $span; ?></span></a>


                  <ul class="treeview-menu">
                   <?php 
                   $c=count($subnavmenuname);
                   for($rs=0;$rs<=$c;$rs++) {
                    $rssubnavmenuname = $subnavmenuname[$rs];
                    echo $rssubnavmenuname; 
                  }
                  ?>  
                </ul>
              </li>
              <?php
              $i++;
            }
          }
          ?>  

        </ul>
      </section>
      <!-- /.sidebar -->
    </aside>